package com.serhii.papercuts.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.serhii.papercuts.R
import com.serhii.papercuts.utils.view.BaseFragment
import com.serhii.papercuts.viewmodel.AuthViewModel
import kotlinx.android.synthetic.main.fragment_sign_up.*
import kotlinx.android.synthetic.main.fragment_sign_up.emailEdit
import kotlinx.android.synthetic.main.fragment_sign_up.errorText
import org.koin.android.viewmodel.ext.android.sharedViewModel

class SignUpFragment : BaseFragment() {

    private val activityVM: AuthViewModel by sharedViewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(R.layout.fragment_sign_up, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupViews()
        setupListeners()
    }

    private fun setupViews() {
        signUpBtn.setOnClickListener {
            if (passEdit.text.toString() != repeatPassEdit.text.toString())
                activityVM.signUpErrorLiveData.postValue("Passwords don't match")
            else
                activityVM.signUpWithEmail(
                    emailEdit.text.toString(),
                    passEdit.text.toString()
                )
        }
//        signInWithFacebookBtn.setOnClickListener { }
//        signInWithGoogleBtn.setOnClickListener {
//            getActivity<AuthActivity>()?.signInWithGoogle()
//        }
        backBtn.setOnClickListener { activity?.onBackPressed() }
    }

    private fun setupListeners() {
        activityVM.signUpErrorLiveData.observe(viewLifecycleOwner, Observer {
            errorText.text = it
            errorText.visibility = View.VISIBLE
        })
    }
}