package com.serhii.papercuts.ui.activities

import android.app.Activity
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.MultiAutoCompleteTextView
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.core.widget.addTextChangedListener
import androidx.recyclerview.widget.LinearLayoutManager
import com.serhii.papercuts.R
import com.serhii.papercuts.managers.SharedPreferencesManager
import com.serhii.papercuts.model.entities.CardEntity
import com.serhii.papercuts.model.entities.DeckEntity
import com.serhii.papercuts.model.entities.MiniCardEntity
import com.serhii.papercuts.ui.adapters.recycler.TagAdapter
import com.serhii.papercuts.utils.extencions.openChooser
import com.serhii.papercuts.utils.extencions.showToast
import com.serhii.papercuts.utils.extencions.toCardsListMethod1
import com.serhii.papercuts.utils.extencions.toCardsListMethod2
import com.serhii.papercuts.utils.view.BaseActivity
import com.serhii.papercuts.viewmodel.CreateCardViewModel
import kotlinx.android.synthetic.main.activity_create_card.*
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import java.io.BufferedReader
import java.io.File
import java.io.FileOutputStream
import java.io.FileReader
import java.lang.Exception

class NewCardActivity : BaseActivity() {

    companion object {
        private const val FRONT_GALLERY_REQUEST = 0
        private const val BACK_GALLERY_REQUEST = 1
        private const val FRONT_CAMERA_REQUEST = 2
        private const val BACK_CAMERA_REQUEST = 3

        private const val SELECT_FILE_REQUEST = 4

        private const val DECK_SELECT = 10

        const val TAGS_KEY = "tags"
        const val CARD_KEY = "card"
    }

    private val viewModel: CreateCardViewModel by viewModel()

    private val sharedPreferencesManager: SharedPreferencesManager by inject()

    private val tagAdapter: TagAdapter by inject()
    private lateinit var tagAutoCompleteAdapter: ArrayAdapter<String>

    private var tags: Array<String>? = null
    private var card: CardEntity? = null
    private var cardsPack = ArrayList<MiniCardEntity>()
    private var isFromFile = false

    private var cardCreated = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_card)
        upgradeStatusBar()

        window.statusBarColor = ContextCompat.getColor(this, R.color.background_ternary)

        tags = intent.getStringArrayExtra(TAGS_KEY)
        val c = intent.getSerializableExtra(CARD_KEY)
        if (c is CardEntity)
            card = c

        setupViews()
        setExtras()
        setupListeners()

//        getDeck()
    }

    private fun setupViews() {
        backBtn.setOnClickListener {
            if(cardCreated)
                setResult(Activity.RESULT_OK)
            finish()
        }

        if (card == null)
            frontEdit.requestFocus()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            intent.getCharSequenceExtra(Intent.EXTRA_PROCESS_TEXT)?.let {
                frontEdit.setText(it)
                backEdit.requestFocus()
            }
        }

        swapBtn.setOnClickListener {
            val front = frontEdit.text.toString()
            val back = backEdit.text.toString()

            frontEdit.setText(back)
            backEdit.setText(front)
        }

        createBtn.setOnClickListener {
            it.isEnabled = false

            if (isFromFile)
                viewModel.addCards(
                    cardsPack,
                    switchableCheckBox.isChecked,
                    tagAdapter.items ?: emptyList()
                )
            else
                addCard()
        }
        pickFileBtn.setOnClickListener { parseFile() }
        discardParseBtn.setOnClickListener {
            toggleFileMode(false)
            cardsPack.clear()
        }

        tagAdapter.closeBtnEnabled = true
        tagAdapter.isTagClickable = false

        tagsRV.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        tagsRV.adapter = tagAdapter

        tagAutoCompleteAdapter =
            ArrayAdapter<String>(this, android.R.layout.simple_expandable_list_item_1)

        tagsAutoEdit.setAdapter(tagAutoCompleteAdapter)
        tagsAutoEdit.setTokenizer(MultiAutoCompleteTextView.CommaTokenizer())
        tagsAutoEdit.onItemClickListener = AdapterView.OnItemClickListener { _, _, i, _ ->
            tagAutoCompleteAdapter.getItem(i)?.let {
                tagAdapter.addTag(it)
                tagsAutoEdit.text.clear()
            }
        }
        tagsAutoEdit.addTextChangedListener {
            if (tagsAutoEdit.length() in 1..3)
                viewModel.searchTags(tagsAutoEdit.text.toString())
        }
    }

    private fun setExtras() {
        card?.let {
            pickFileBtn.isVisible = false
            actionBarTitle.setText(R.string.edit_card)
            frontEdit.setText(it.frontText)
            backEdit.setText(it.backText)
            createBtn.setText(R.string.update)
            createBtn.setOnClickListener { v ->
                v.isEnabled = false
                viewModel.updateCard(
                    frontEdit.text.toString(),
                    backEdit.text.toString(),
                    it,
                    tagAdapter.items ?: emptyList()
                )
            }
        }
        tags?.forEach {
            tagAdapter.addTag(it)
        }
    }

    private fun setupListeners() {
        viewModel.uploadSuccess.observe(this, {
            if (it) {
                cardCreated = true
                if (card != null)
                    finish()
                else
                    resetDialog()
            }
        })
        viewModel.selectedDeckLiveData.observe(this, {
//            deckBtn.text = it.name
        })
        viewModel.tagsLiveData.observe(this, {
//            tagAdapter.setItems(it)

            tagAutoCompleteAdapter.clear()
            tagAutoCompleteAdapter.addAll(it)
        })
    }

    private fun addCard() {
        val frontArray = frontEdit.text.toString().split(", ")
        val backArray = backEdit.text.toString().split(", ")

        if (frontArray.size > 1) {
            val cards = ArrayList<MiniCardEntity>()
            for (i in frontArray.indices) {
                cards.add(
                    MiniCardEntity(
                        frontArray[i],
                        backArray[i]
                    )
                )
            }
            viewModel.addCards(
                cards,
                switchableCheckBox.isChecked,
                tagAdapter.items ?: emptyList()
            )
        } else
            viewModel.addCard(
                frontArray[0],
                backArray[0],
                switchableCheckBox.isChecked,
                tagAdapter.items ?: emptyList()
            )
    }

    private fun toggleFileMode(value: Boolean) {
        isFromFile = value
        frontEditLayout.isVisible = !value
        backEditLayout.isVisible = !value
        swapBtn.isVisible = !value
        fileLayout.isVisible = value
    }

//    private fun getDeck() {
//        intent.extras?.get("deck")?.let {
//            if (it is DeckEntity)
//                viewModel.selectedDeckLiveData.value = it
//        }
//    }

    private fun resetDialog() {
        viewModel.frontIsImg = false
        viewModel.backIsImg = false

        frontEdit.isEnabled = true
        frontEdit.text?.clear()

        backEdit.isEnabled = true
        backEdit.text?.clear()

        fileLayout.isVisible = false
        frontEditLayout.isVisible = true
        backEditLayout.isVisible = true

        createBtn.isEnabled = true
    }

    private fun parseFile() {
        if (!sharedPreferencesManager.getUserSeenFileDialog())
            AlertDialog.Builder(this)
                .setTitle(getString(R.string.parse_dialog_title))
                .setMessage("Pick a .txt file with one card per line. Front and back sides of the card must be separated by \" -- \".\nExample:\n\nfront text 1 -- back text 1\nfront text 2 -- back text 2\n")
                .setPositiveButton(R.string.ok) { _, _ ->
                    openChooser(
                        SELECT_FILE_REQUEST,
                        "text/plain"
                    )
                }
                .show()
        else
            openChooser(SELECT_FILE_REQUEST, "text/plain")
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK)
            when (requestCode) {
                SELECT_FILE_REQUEST -> {
                    data?.data?.let {
                        val file = File(cacheDir, "t.txt")
                        val fis = contentResolver.openInputStream(it)
                        val fos = FileOutputStream(file)
                        fis?.copyTo(fos)
                        fos.flush()

                        var miniCards = file.toCardsListMethod1()
                        if (miniCards.isEmpty())
                            miniCards = file.toCardsListMethod2()

                        if (miniCards.isEmpty()) {
                            showToast(getString(R.string.wrong_parse_format))
                        } else {
                            cardsPack = miniCards
                            toggleFileMode(true)
                            fileTxt.text =
                                getString(R.string.parsed_cards_from_file, miniCards.size)
                        }
                    }
                }
                FRONT_GALLERY_REQUEST -> {
                }
                BACK_GALLERY_REQUEST -> {
                }
                DECK_SELECT -> {
                    data?.extras?.get("deck")?.let {
                        if (it is DeckEntity)
                            viewModel.selectedDeckLiveData.value = it
                    }
                }
            }
    }

    override fun finish() {
        val data = Intent()
        setResult(RESULT_OK, data)
        super.finish()
    }
}