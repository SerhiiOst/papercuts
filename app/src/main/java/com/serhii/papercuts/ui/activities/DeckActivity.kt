package com.serhii.papercuts.ui.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.Gravity
import android.view.View
import android.widget.PopupMenu
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.serhii.papercuts.R
import com.serhii.papercuts.model.entities.CardEntity
import com.serhii.papercuts.model.entities.DeckEntity
import com.serhii.papercuts.ui.adapters.recycler.CardAdapter
import com.serhii.papercuts.ui.adapters.recycler.DeckCardAdapter
import com.serhii.papercuts.utils.extencions.setupIcons
import com.serhii.papercuts.utils.view.BaseActivity
import com.serhii.papercuts.viewmodel.DeckViewModel
import kotlinx.android.synthetic.main.activity_deck.*
import kotlinx.android.synthetic.main.activity_deck.actionBarTitle
import kotlinx.android.synthetic.main.activity_deck.addSelectedToDeckBtn
import kotlinx.android.synthetic.main.activity_deck.backBtn
import kotlinx.android.synthetic.main.activity_deck.cardsRV
import kotlinx.android.synthetic.main.activity_deck.deleteSelectedBtn
import kotlinx.android.synthetic.main.activity_deck.labelSelectedBtn
import kotlinx.android.synthetic.main.activity_deck.selectedBar
import kotlinx.android.synthetic.main.item_check_box.view.*
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel

class DeckActivity : BaseActivity() {

    companion object {
        const val DECK_KEY = "deck"

        const val DECK_NAME_KEY = "name"
        const val DECK_DESCRIPTION_KEY = "description"

        const val CARDS_ADDED_CODE = 1
        const val DECK_UPDATE_CODE = 2
    }

    private val viewModel: DeckViewModel by viewModel()

    private val cardAdapter: DeckCardAdapter by inject()

    private var isSelectModeActive = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        upgradeStatusBar()
        setContentView(R.layout.activity_deck)

        window.statusBarColor = ContextCompat.getColor(this, R.color.background_secondary)

        setExtras()
        setupViews()
        setupRV()
        setupListeners()

        viewModel.getDeckCards()
    }

    private fun setExtras() {
        intent.getSerializableExtra(DECK_KEY)?.let {
            if (it is DeckEntity) {
                name.text = it.name
                description.text = it.description
                description.isVisible = it.description.isNotEmpty()

                viewModel.deckLiveData.value = it
            }
        }

//        name.text = intent.getStringExtra(DECK_NAME_KEY)
//        val des = intent.getStringExtra(DECK_DESCRIPTION_KEY)
//        description.text = des
//        description.isVisible = des?.isNotEmpty() == true
    }

    private fun setupViews() {
        backBtn.setOnClickListener { onBackPressed() }

        addBtn.setOnClickListener {
            viewModel.deckLiveData.value?.id?.let { deckId ->
                startActivityForResult(
                    Intent(this, SelectCardsActivity::class.java).also { i ->
                        i.putExtra(SelectCardsActivity.DECK_ID_KEY, deckId)
                    },
                    CARDS_ADDED_CODE
                )
            }
        }
        studyFab.setOnClickListener {
            val i = Intent(this, StudyActivity::class.java).also { i ->
                i.putExtra(StudyActivity.DECK_KEY, viewModel.deckLiveData.value)
            }
            startActivity(i)
        }
        menuBtn.setOnClickListener {
            val popup = PopupMenu(this, menuAnchor, Gravity.TOP)
            popup.setupIcons()
            popup.menuInflater.inflate(R.menu.deck, popup.menu)
            popup.setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.progress -> {
                        viewModel.cardsLiveData.value?.let { cards ->
                            val i = Intent(this, ProgressActivity::class.java).also { i ->
                                val list: ArrayList<Int> = ArrayList(cards.map { c -> c.progress })
                                i.putExtra(ProgressActivity.PROGRESS_ARRAY_KEY, list)
                            }
                            startActivity(i)
                        }
                    }
                    R.id.edit -> editDeck()
                    R.id.delete -> deleteDeck()
                }
                return@setOnMenuItemClickListener true
            }
            popup.show()
        }

        //GAMES
        gameRelationBtn.setOnClickListener { goToRelationGame() }

        //SELECT MODE
        cancelSelectBtn.setOnClickListener { toggleSelectMode(false) }
        labelSelectedBtn.setOnClickListener {
            startActivity(Intent(this, LabelCardsActivity::class.java).also {
                it.putExtra(
                    "cards",
                    cardAdapter.selectedCards
                )
            })
            delayedCancelSelectMode()
        }
        addSelectedToDeckBtn.setOnClickListener {
            addCardsToDeck(ArrayList(cardAdapter.selectedCards.map { c -> c.id }))
            delayedCancelSelectMode()
        }
        removeSelectedBtn.setOnClickListener {
            removeCards(*cardAdapter.selectedCards.toTypedArray())
        }
        deleteSelectedBtn.setOnClickListener {
            deleteCards(*cardAdapter.selectedCards.toTypedArray())
        }
    }

    private fun setupRV() {
        cardsRV.layoutManager =
            StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL)

        cardsRV.adapter = cardAdapter

        cardAdapter.onDeleteCard = {
            deleteCards(it)
        }
        cardAdapter.onRemoveCard = {
            removeCards(it)
        }
        cardAdapter.onEditCard = {
            startActivityForResult(
                Intent(this, NewCardActivity::class.java).also { intent ->
                    intent.putExtra(
                        NewCardActivity.TAGS_KEY,
                        it.tags.toTypedArray()
                    )
                    intent.putExtra(NewCardActivity.CARD_KEY, it)
                }, 1
            )
        }
        cardAdapter.onAddToDeck = {
            addCardsToDeck(arrayListOf(it.id))
        }
        cardAdapter.onSelectCard = {
            toggleSelectMode(true)
        }
        cardAdapter.onSelectedCardsSizeChanged = {
            actionBarTitle.text = getString(R.string.selected_number, it)
            if (it == 0)
                toggleSelectMode(false)
        }
        cardsRV.adapter = cardAdapter
    }

    private fun setupListeners() {
        viewModel.cardsLiveData.observe(this, {
            cardAdapter.items = it
        })
//        viewModel.removedCardLiveData.observe(this, {
//            val c = cardAdapter.items?.find { c -> c.id == it }
//            cardAdapter.items?.indexOf(c)?.let {
//                cardAdapter.items = cardAdapter.items?.filterNot { card -> card == c }
//                cardAdapter.notifyItemRemoved(it)
//            }
//        })
        viewModel.removedCardLiveData.observe(this, {
            cardAdapter.items = cardAdapter.items?.filterNot { c -> it.contains(c.id) }
            cardAdapter.notifyDataSetChanged()
        })
        viewModel.deckDeletedLiveData.observe(this, {
            if (it) {
                val deck = viewModel.deckLiveData.value?.id
                setResult(Activity.RESULT_OK, Intent().also { i -> i.putExtra(DECK_KEY, deck) })
                finish()
            }
        })
    }

    private fun removeCards(vararg cards: CardEntity) {
        MaterialAlertDialogBuilder(this)
            .setTitle(R.string.remove)
            .setMessage(R.string.are_you_sure)
            .setPositiveButton(R.string.ok) { _, _ ->
                viewModel.removeCardsFromDeck(cards.map { c -> c.id })
                toggleSelectMode(false)
            }
            .setNegativeButton(R.string.cancel) { _, _ -> }
            .show()
    }

    private fun deleteCards(vararg cards: CardEntity) {
        val v = View.inflate(this, R.layout.item_check_box, null)
        val checkBox = v.checkBox
        checkBox.text = getString(R.string.delete_from_everyone)

        val builder = MaterialAlertDialogBuilder(this)
        if (cards.all { it.isAuthor == true })
            builder.setView(v)
        builder
            .setTitle(R.string.delete)
            .setMessage(R.string.are_you_sure)
            .setPositiveButton(R.string.ok) { _, _ ->
                val isGlobally = checkBox.isChecked
                viewModel.deleteCards(cards.map { c -> c.id }.toList(), isGlobally)
                toggleSelectMode(false)
            }
            .setNegativeButton(R.string.cancel) { _, _ -> }
            .show()
    }

    private fun addCardsToDeck(list: ArrayList<String>) {
        val i = Intent(this, SelectDeckActivity::class.java).also { i ->
            i.putExtra(SelectDeckActivity.CARD_IDS_KEY, list)
        }
        startActivity(i)
    }

    private fun delayedCancelSelectMode() {
        Handler(Looper.getMainLooper()).postDelayed({
            toggleSelectMode(false)
        }, 300)
    }

    private fun toggleSelectMode(value: Boolean) {
        isSelectModeActive = value

        selectedBar.isVisible = value
//        collapsingToolBar.isVisible = !value
        description.isVisible = !value && description.text.isNotEmpty()
        deckActionBar.isVisible = !value

        if (!value)
            cardAdapter.cancelSelectMode()
    }

    private fun editDeck() {
        val i = Intent(this, NewDeckActivity::class.java).also { i ->
            viewModel.deckLiveData.value?.let {
                i.putExtra(NewDeckActivity.DECK_KEY, it)
            }
        }
        startActivityForResult(i, DECK_UPDATE_CODE)
    }

    private fun deleteDeck() {
        MaterialAlertDialogBuilder(this)
            .setTitle(R.string.delete)
            .setMessage(R.string.are_you_sure)
            .setPositiveButton(R.string.ok) { _, _ ->
                viewModel.deleteDeck()
            }
            .setNegativeButton(R.string.cancel) { _, _ -> }
            .show()
    }

    private fun goToRelationGame() {
        viewModel.deckLiveData.value?.id?.let {
            val i = Intent(this, GameRelationActivity::class.java)
            i.putExtra(GameRelationActivity.DECK_ID_KEY, it)
            startActivity(i)
        }
    }

    override fun onBackPressed() {
        if (isSelectModeActive)
            toggleSelectMode(false)
        else
            super.onBackPressed()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK)
            when (requestCode) {
                CARDS_ADDED_CODE -> viewModel.getDeckCards()
                DECK_UPDATE_CODE -> {
                    data?.let {
                        val n = data.getStringExtra(NewDeckActivity.DECK_NAME_KEY)
                        val d = data.getStringExtra(NewDeckActivity.DECK_DESCRIPTION_KEY)

                        if (n != null && d != null) {
                            name.text = n
                            description.text = d
                            description.isVisible = d.isNotEmpty()
                        }
                    }
                }
            }
    }
}