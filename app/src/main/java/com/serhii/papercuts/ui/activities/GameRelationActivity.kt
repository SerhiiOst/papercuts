package com.serhii.papercuts.ui.activities

import android.animation.AnimatorInflater
import android.animation.AnimatorSet
import android.os.Bundle
import android.os.Handler
import androidx.core.animation.doOnEnd
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.serhii.papercuts.R
import com.serhii.papercuts.model.entities.GameRelationOptionEntity
import com.serhii.papercuts.model.entities.RelationOptionState
import com.serhii.papercuts.ui.adapters.recycler.GameRelationOptionAdapter
import com.serhii.papercuts.utils.extencions.onThrottleClick
import com.serhii.papercuts.utils.view.BaseActivity
import com.serhii.papercuts.viewmodel.GameRelationViewModel
import kotlinx.android.synthetic.main.activity_game_relation.*
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel

class GameRelationActivity : BaseActivity() {

    companion object {
        const val DECK_ID_KEY = "deck_id"

        const val MAX_ITEM_COUNT = 10
    }

    private val viewModel: GameRelationViewModel by viewModel()

    private val termsAdapter: GameRelationOptionAdapter by inject()
    private val definitionsAdapter: GameRelationOptionAdapter by inject()

    var deckId = ""

    var itemCount = MAX_ITEM_COUNT
    var score = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        upgradeStatusBar()
        setContentView(R.layout.activity_game_relation)

        window.statusBarColor = ContextCompat.getColor(this, R.color.background_secondary)

        setExtras()
        setupViews()
        setupRV()
        setupListeners()
    }

    private fun setExtras() {
        intent.getStringExtra(DECK_ID_KEY)?.let {
            deckId = it
            getData()
        }
    }

    private fun setupViews() {
        backBtn.setOnClickListener { onBackPressed() }
        playAgainBtn.onThrottleClick {
            val animation = AnimatorInflater.loadAnimator(
                this@GameRelationActivity,
                R.animator.fade
            ) as AnimatorSet
            animation.setTarget(playAgainBtn)
            animation.start()
            animation.doOnEnd {
                playAgainBtn.isVisible = false
            }

            gameLayout.isVisible = true

            getData()
        }
    }

    private fun setupRV() {
        termsRV.layoutManager = LinearLayoutManager(this)
        definitionsRV.layoutManager = LinearLayoutManager(this)

        termsRV.adapter = termsAdapter
        definitionsRV.adapter = definitionsAdapter

        termsAdapter.onClickFun = {
            var state = RelationOptionState.IDLE
            val firstSelected = definitionsAdapter.selectedEntity == null
            if (firstSelected)
                state = RelationOptionState.SELECTED
            else {
                viewModel.cardsLiveData.value?.let { cards ->
                    state =
                        if (cards.find { c -> c.frontText == it.value }?.backText == definitionsAdapter.selectedEntity!!.value)
                            RelationOptionState.RIGHT
                        else
                            RelationOptionState.WRONG
                    definitionsAdapter.selectedEntity?.state = state
                    if (state == RelationOptionState.RIGHT) {
                        score++
                        checkOnWin()
                    }
                }
            }
            it.state = state
            termsAdapter.notifySelectedItem()
            definitionsAdapter.notifySelectedItem()
            if (!firstSelected) {
                definitionsAdapter.selectedEntity = null
                termsAdapter.selectedEntity = null
            }
        }

        definitionsAdapter.onClickFun = {
            var state = RelationOptionState.IDLE
            val firstSelected = termsAdapter.selectedEntity == null
            if (firstSelected)
                state = RelationOptionState.SELECTED
            else {
                viewModel.cardsLiveData.value?.let { cards ->
                    //TODO several options can have same values
                    state =
                        if (cards.find { c -> c.backText == it.value }?.frontText == termsAdapter.selectedEntity!!.value)
                            RelationOptionState.RIGHT
                        else
                            RelationOptionState.WRONG
                    termsAdapter.selectedEntity?.state = state
                    if (state == RelationOptionState.RIGHT) {
                        score++
                        checkOnWin()
                    }
                }
            }
            it.state = state
            termsAdapter.notifySelectedItem()
            definitionsAdapter.notifySelectedItem()
            if (!firstSelected) {
                definitionsAdapter.selectedEntity = null
                termsAdapter.selectedEntity = null
            }
        }
    }

    private fun setupListeners() {
        viewModel.cardsLiveData.observe(this, {
            itemCount = it.size
            termsAdapter.items = it.map { c -> GameRelationOptionEntity(c.frontText) }.shuffled()
            definitionsAdapter.items =
                it.map { c -> GameRelationOptionEntity(c.backText) }.shuffled()
        })
    }

    private fun getData() {
        viewModel.getDeckCards(deckId)
    }

    private fun checkOnWin() {
        if (score == itemCount) {
            playAgainBtn.isVisible = true
            playAgainBtn.alpha = 0f
            val animation = AnimatorInflater.loadAnimator(
                this@GameRelationActivity,
                R.animator.appear
            ) as AnimatorSet
            animation.setTarget(playAgainBtn)
            Handler().postDelayed({
                animation.start()
                gameLayout.isVisible = false
            }, 1000)
        }

    }
}