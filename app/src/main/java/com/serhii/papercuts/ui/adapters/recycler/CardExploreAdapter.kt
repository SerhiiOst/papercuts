package com.serhii.papercuts.ui.adapters.recycler

import android.content.Context
import android.view.Gravity
import android.view.View
import android.widget.PopupMenu
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.serhii.papercuts.R
import com.serhii.papercuts.model.entities.CardEntity
import com.serhii.papercuts.utils.BaseAdapter
import com.serhii.papercuts.utils.view.FlashCardView
import com.serhii.papercuts.utils.view.GlobalFlashCardView
import org.koin.ext.getOrCreateScope
import java.lang.reflect.Field
import java.lang.reflect.Method

class CardExploreAdapter : BaseAdapter<CardEntity>() {

    override val layoutId = R.layout.item_card_global

    var onAddCard: ((CardEntity) -> Unit)? = null

    override fun bind(itemView: View, entity: CardEntity, position: Int) {
        if (itemView is GlobalFlashCardView)
            with(itemView) {
                cardEntity = entity
                itemView.onLikeClick = {
                    onAddCard?.invoke(entity)
                }
                onClick = {
                    flipFun.invoke()
                }
                onLongClick = {
                    true
                }
            }
    }

    fun refreshCard(card: CardEntity) {
        items?.indexOfFirst { it == card }?.let {
            notifyItemChanged(it)
        }
    }
}