package com.serhii.papercuts.ui.activities

import android.animation.AnimatorInflater
import android.animation.AnimatorSet
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import com.google.android.material.snackbar.Snackbar
import com.serhii.papercuts.R
import com.serhii.papercuts.model.entities.CardEntity
import com.serhii.papercuts.model.entities.DeckEntity
import com.serhii.papercuts.ui.adapters.recycler.StudyAdapter
import com.serhii.papercuts.utils.view.BaseActivity
import com.serhii.papercuts.viewmodel.StudyViewModel
import com.yuyakaido.android.cardstackview.*
import kotlinx.android.synthetic.main.activity_study.*
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel

class StudyActivity : BaseActivity() {

    companion object {
        const val TAGS_KEY = "tags"
        const val DECK_KEY = "deck"
        const val ARCHIVE_KEY = "archive"
    }

    private val viewModel: StudyViewModel by viewModel()

    private val cardAdapter: StudyAdapter by inject()

    private var query = ""
    private var deck: DeckEntity? = null
    private var isDeck = false
    private var isArchive = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        upgradeStatusBar()
        setContentView(R.layout.activity_study)

        setExtras()
        setupViews()
        setupRV()
        setupListeners()

        getData()
    }

    private fun setExtras() {
        query = intent.getStringExtra(TAGS_KEY) ?: ""
        filterTxt.text = if (query.isEmpty()) getString(R.string.all_cards) else query

        isArchive = intent.getBooleanExtra(ARCHIVE_KEY, false)
        intent.getSerializableExtra(DECK_KEY)?.let {
            if (it is DeckEntity) {
                isDeck = true
                deck = it
                filterTxt.text = it.name
            }
        }
    }

    private fun setupViews() {
        closeBtn.setOnClickListener { finish() }
        refreshBtn.setOnClickListener {
            val animation = AnimatorInflater.loadAnimator(
                this@StudyActivity,
                R.animator.fade
            ) as AnimatorSet
            animation.setTarget(noCardsLayout)
            animation.start()

            cardsRV.isVisible = true

            getData()
        }
    }

    private fun setupRV() {
        val cardStackManager = CardStackLayoutManager(
            this,
            object : CardStackListener {
                override fun onCardDragging(direction: Direction?, ratio: Float) {}
                override fun onCardRewound() {}
                override fun onCardCanceled() {}
                override fun onCardDisappeared(view: View?, position: Int) {}
                override fun onCardAppeared(view: View?, position: Int) {}

                override fun onCardSwiped(direction: Direction?) {
                    direction?.let {
                        this@StudyActivity.cardAdapter.swipe()?.let { card ->
                            val rewindSettings = RewindAnimationSetting.Builder()
                                .setDirection(direction)
                                .build()
                            if(cardsRV.layoutManager is CardStackLayoutManager)
                                (cardsRV.layoutManager as CardStackLayoutManager).setRewindAnimationSetting(rewindSettings)
                            dismissCard(direction == Direction.Right, card)
                        }
                        if (cardAdapter.isCompleted()) {
                            cardsRV.isVisible = false
                            noCardsLayout.alpha = 0f
                            val animation = AnimatorInflater.loadAnimator(
                                this@StudyActivity,
                                R.animator.appear
                            ) as AnimatorSet
                            animation.setTarget(noCardsLayout)
                            animation.start()
                        }
                    }
                }
            }
        )
        cardStackManager.setCanScrollVertical(false)
        cardStackManager.setMaxDegree(40f)
        cardStackManager.setStackFrom(StackFrom.None)
        cardStackManager.setVisibleCount(1)
        cardStackManager.setTranslationInterval(8f)

        cardsRV.layoutManager = cardStackManager
        cardsRV.adapter = cardAdapter

        //TODO
        cardAdapter.onDeleteCard = {
//            deleteCards(it)
        }
        cardAdapter.onEditCard = {
//            startActivityForResult(
//                Intent(requireContext(), NewCardActivity::class.java).also { intent ->
//                    intent.putExtra(
//                        "tags",
//                        it.tags.toTypedArray()
//                    )
//                    intent.putExtra("card", it)
//                }, 1
//            )
        }
    }

    private fun setupListeners() {
        viewModel.cardsLiveData.observe(this, {
            cardAdapter.refresh()
            cardAdapter.items = it
        })
    }

    private fun dismissCard(isRemembered: Boolean, card: CardEntity) {
        val snackText = if(isRemembered) R.string.card_was_remembered else R.string.card_wasnt_remembered
        Snackbar.make(rootLayout, getText(snackText), Snackbar.LENGTH_LONG)
            .setBackgroundTint(ContextCompat.getColor(this, R.color.background_secondary))
            .setTextColor(ContextCompat.getColor(this, R.color.text_color))
            .setAnchorView(swipeLeftInfo)
            .addCallback(object : Snackbar.Callback() {
                override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                    super.onDismissed(transientBottomBar, event)
                    if (event != DISMISS_EVENT_ACTION) {
                        if (isRemembered)
                            viewModel.rememberCard(card)
                        else
                            viewModel.forgetCard(card)
                    }
                }
            })
            .setAction(getText(R.string.undo)) {
                rewind()
            }
            .show()
    }

    private fun rewind() {
        if (cardAdapter.isCompleted()) {
            cardsRV.isVisible = true
            noCardsLayout.alpha = 0f
//            val animation = AnimatorInflater.loadAnimator(
//                this@StudyActivity,
//                R.animator.appear
//            ) as AnimatorSet
//            animation.setTarget(noCardsLayout)
//            animation.start()
        }
        cardsRV.rewind()
        cardAdapter.rewind()
    }

    private fun getData() {
        when {
            isArchive -> viewModel.getArchivedCards()
            isDeck -> viewModel.getDeckCards(deck?.id ?: "")
            else -> viewModel.searchUserCards(query)
        }
    }
}