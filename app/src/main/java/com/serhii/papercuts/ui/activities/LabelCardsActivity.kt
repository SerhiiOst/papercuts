package com.serhii.papercuts.ui.activities

import android.app.Activity
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.MultiAutoCompleteTextView
import androidx.core.widget.addTextChangedListener
import androidx.recyclerview.widget.LinearLayoutManager
import com.serhii.papercuts.R
import com.serhii.papercuts.model.entities.CardEntity
import com.serhii.papercuts.ui.adapters.recycler.TagAdapter
import com.serhii.papercuts.utils.view.BaseActivity
import com.serhii.papercuts.viewmodel.CreateCardViewModel
import kotlinx.android.synthetic.main.activity_create_card.*
import kotlinx.android.synthetic.main.activity_label_cards.*
import kotlinx.android.synthetic.main.activity_label_cards.actionBarTitle
import kotlinx.android.synthetic.main.activity_label_cards.backBtn
import kotlinx.android.synthetic.main.activity_label_cards.tagsAutoEdit
import kotlinx.android.synthetic.main.activity_label_cards.tagsRV
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel

class LabelCardsActivity : BaseActivity() {

    private val viewModel: CreateCardViewModel by viewModel()

    private val tagAdapter: TagAdapter by inject()
    private lateinit var tagAutoCompleteAdapter: ArrayAdapter<String>

    private var cards: List<CardEntity>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_label_cards)
        upgradeStatusBar()

        cards = intent.getSerializableExtra("cards") as (List<CardEntity>)

        setupViews()
        setExtras()
        setupListeners()
    }

    private fun setupViews() {
        backBtn.setOnClickListener { finish() }

        tagAdapter.closeBtnEnabled = true
        tagAdapter.isTagClickable = false

        tagsAutoEdit.requestFocus()

        tagsRV.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        tagsRV.adapter = tagAdapter

        tagAutoCompleteAdapter =
            ArrayAdapter<String>(this, android.R.layout.simple_expandable_list_item_1)

        tagsAutoEdit.setAdapter(tagAutoCompleteAdapter)
        tagsAutoEdit.setTokenizer(MultiAutoCompleteTextView.CommaTokenizer())
        tagsAutoEdit.onItemClickListener = AdapterView.OnItemClickListener { _, _, i, _ ->
            tagAutoCompleteAdapter.getItem(i)?.let {
                tagAdapter.addTag(it)
                tagsAutoEdit.text.clear()
            }
        }
        tagsAutoEdit.addTextChangedListener {
            if (tagsAutoEdit.length() in 1..3)
                viewModel.searchTags(tagsAutoEdit.text.toString())
        }
    }

    private fun setExtras() {
        cards?.let {
            applyBtn.setOnClickListener { _ ->
                it.forEach { c ->
                    val tList = ArrayList<String>()
                    tList.addAll(c.tags)
                    tList.addAll(tagAdapter.items ?: emptyList())
                    viewModel.updateCard(
                        c.frontText,
                        c.backText,
                        c,
                        tList
                    )
                }
            }
        }
    }

    private fun setupListeners() {
        viewModel.uploadSuccess.observe(this, {
            finish()
        })
        viewModel.tagsLiveData.observe(this, {
            tagAutoCompleteAdapter.clear()
            tagAutoCompleteAdapter.addAll(it)
        })
    }
}