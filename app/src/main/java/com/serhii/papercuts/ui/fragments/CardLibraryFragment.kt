package com.serhii.papercuts.ui.fragments

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.serhii.papercuts.R
import com.serhii.papercuts.model.entities.CardEntity
import com.serhii.papercuts.ui.activities.LabelCardsActivity
import com.serhii.papercuts.ui.activities.NewCardActivity
import com.serhii.papercuts.ui.activities.SelectDeckActivity
import com.serhii.papercuts.ui.adapters.recycler.CardAdapter
import com.serhii.papercuts.ui.adapters.recycler.TagAdapter
import com.serhii.papercuts.utils.view.BaseFragment
import com.serhii.papercuts.viewmodel.LibraryViewModel
import kotlinx.android.synthetic.main.fragment_library_cards.*
import kotlinx.android.synthetic.main.item_check_box.view.*
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.sharedViewModel

class CardLibraryFragment : BaseFragment() {

    private val activityVM: LibraryViewModel by sharedViewModel()

    private val searchTagAdapter: TagAdapter by inject()
    private val libraryTagAdapter: TagAdapter by inject()
    private val cardAdapter: CardAdapter by inject()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(R.layout.fragment_library_cards, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupViews()
        setupTagsRV()
        setupCardRV()
        setupListeners()
    }

    private fun setupViews() {
        swipeRefresh.setOnRefreshListener {
            refresh()
        }
        backBtn.setOnClickListener { activity?.onBackPressed() }
//        searchEdit.doOnTextChanged { text, start, before, count ->
//            activityVM.searchUserCards(text.toString())
//        }

        //SELECT MODE
        selectAllBtn.setOnClickListener { }
        labelSelectedBtn.setOnClickListener {
            startActivity(Intent(requireContext(), LabelCardsActivity::class.java).also {
                it.putExtra(
                    "cards",
                    cardAdapter.selectedCards
                )
            })
            delayedCancelSelectMode()
        }
        addSelectedToDeckBtn.setOnClickListener {
            addCardsToDeck(ArrayList(cardAdapter.selectedCards.map { c -> c.id }))
            delayedCancelSelectMode()
        }
        deleteSelectedBtn.setOnClickListener {
            deleteCards(*cardAdapter.selectedCards.toTypedArray())
        }
    }

    private fun setupTagsRV() {
        searchTagsRV.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        libraryTagsRV.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)

        searchTagAdapter.isTagClickable = false
        searchTagAdapter.closeBtnEnabled = true
        searchTagAdapter.onCloseClickFun = {
            libraryTagAdapter.addTag(it)
            activityVM.searchTags.remove(it)
            activityVM.getUserCards()
        }
        searchTagsRV.adapter = searchTagAdapter

        libraryTagAdapter.onItemClickFun = {
            searchTagAdapter.addTag(it)
            activityVM.searchTags.add(it)
            activityVM.getUserCards()
        }
        libraryTagsRV.adapter = libraryTagAdapter
    }

    private fun setupCardRV() {
        cardsRV.layoutManager =
            StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL)

        cardAdapter.onDeleteCard = {
            deleteCards(it)
        }
        cardAdapter.onEditCard = {
            activity?.startActivityForResult(
                Intent(requireContext(), NewCardActivity::class.java).also { intent ->
                    intent.putExtra(
                        "tags",
                        it.tags.toTypedArray()
                    )
                    intent.putExtra("card", it)
                }, 1
            )
        }
        cardAdapter.onAddToDeck = {
            addCardsToDeck(arrayListOf(it.id))
        }
        cardAdapter.onSelectCard = {
            activityVM.selectModeLiveData.value = true
        }
        cardAdapter.onSelectedCardsSizeChanged = {
            actionBarTitle.text = context?.getString(R.string.selected_number, it)
            if (it == 0)
                activityVM.selectModeLiveData.value = false
        }
        cardsRV.adapter = cardAdapter
    }

    private fun setupListeners() {
        activityVM.tagsLiveData.observe(viewLifecycleOwner, {
            libraryTagAdapter.items =
                it.filterNot { t -> searchTagAdapter.items?.contains(t) == true }
            libraryTagAdapter.notifyDataSetChanged()
        })
        activityVM.cardsLiveData.observe(viewLifecycleOwner, {
            swipeRefresh.isRefreshing = false
            cardAdapter.items = it
        })
        activityVM.selectModeLiveData.observe(viewLifecycleOwner, {
            toggleSelectMode(it)
        })
        activityVM.deletedCardLiveData.observe(viewLifecycleOwner, {
            val c = cardAdapter.items?.find { c -> c.id == it }
            cardAdapter.items?.indexOf(c)?.let {
                cardAdapter.items = cardAdapter.items?.filterNot { card -> card == c }
                cardAdapter.notifyItemRemoved(it)
            }
        })
    }

    private fun delayedCancelSelectMode() {
        Handler(Looper.getMainLooper()).postDelayed({
            activityVM.selectModeLiveData.value = false
        }, 300)
    }

    private fun addCardsToDeck(list: ArrayList<String>) {
        val i = Intent(context, SelectDeckActivity::class.java).also { i ->
            i.putExtra(SelectDeckActivity.CARD_IDS_KEY, list)
        }
        startActivity(i)
    }

    private fun deleteCards(vararg cards: CardEntity) {
        val v = View.inflate(context, R.layout.item_check_box, null)
        val checkBox = v.checkBox
        checkBox.text = getString(R.string.delete_from_everyone)

        val builder = MaterialAlertDialogBuilder(requireContext())
        if (cards.all { it.isAuthor == true })
            builder.setView(v)
        builder
            .setTitle(R.string.delete)
            .setMessage(R.string.are_you_sure)
            .setPositiveButton(R.string.ok) { _, _ ->
                val isGlobally = checkBox.isChecked
                activityVM.deleteCards(cards.map { c -> c.id }.toList(), isGlobally)
                cancelSelectMode()
            }
            .setNegativeButton(R.string.cancel) { _, _ -> }
            .show()
    }

    private fun cancelSelectMode() {
        if (activityVM.selectModeLiveData.value == true)
            activityVM.selectModeLiveData.value = false
    }

    private fun toggleSelectMode(value: Boolean) {
        tagsBar.isVisible = !value
        swipeRefresh.isEnabled = !value
        selectedBar.isVisible = value

        if (!value)
            cardAdapter.cancelSelectMode()
    }

    override fun refresh() {
        activityVM.getUserCards()
        activityVM.getUserTags()
    }
}