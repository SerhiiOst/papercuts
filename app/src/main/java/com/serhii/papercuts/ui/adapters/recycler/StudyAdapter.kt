package com.serhii.papercuts.ui.adapters.recycler

import android.animation.AnimatorInflater
import android.animation.AnimatorSet
import android.content.Context
import android.view.Gravity
import android.view.View
import android.widget.PopupMenu
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.serhii.papercuts.R
import com.serhii.papercuts.model.entities.CardEntity
import com.serhii.papercuts.utils.BaseAdapter
import com.serhii.papercuts.utils.view.FlashCardView
import com.serhii.papercuts.utils.view.SwappableFlashCardView
import kotlinx.android.synthetic.main.item_card_study.view.*
import java.lang.reflect.Method

class StudyAdapter : BaseAdapter<CardEntity>() {

    override val layoutId = R.layout.item_card_study

    var onEditCard: ((CardEntity) -> Unit)? = null
    var onDeleteCard: ((CardEntity) -> Unit)? = null

    private var currentIndex = 0

    fun swipe(): CardEntity? {
        items?.get(currentIndex)?.let {
            currentIndex++
            return it
        }
        return null
    }

    fun rewind() {
        if (currentIndex > 0)
            currentIndex--
    }

    fun refresh() {
        currentIndex = 0
    }

    fun isCompleted() = currentIndex == items?.size

    override fun bind(itemView: View, entity: CardEntity, position: Int) {
        with(itemView.flashCardView as FlashCardView) {
            resetCard(entity.randomSides == true)
            itemView.alpha = 0f
            val animation = AnimatorInflater.loadAnimator(
                itemView.context,
                R.animator.appear
            ) as AnimatorSet
            animation.setTarget(itemView)
            animation.start()
            cardEntity = entity

            onLongClick = {
                showContextMenu(context, this, entity)
                true
            }
        }
    }

    private fun showContextMenu(context: Context, anchor: FlashCardView, entity: CardEntity) {
        val popup = PopupMenu(context, anchor, Gravity.TOP)
        try {
            val fields = popup.javaClass.declaredFields
            for (field in fields) {
                if ("mPopup" == field.name) {
                    field.isAccessible = true
                    val menuPopupHelper = field.get(popup)
                    val classPopupHelper = Class.forName(
                        menuPopupHelper
                            .javaClass.name
                    )
                    val setForceIcons: Method = classPopupHelper.getMethod(
                        "setForceShowIcon", Boolean::class.javaPrimitiveType
                    )
                    setForceIcons.invoke(menuPopupHelper, true)
                    break
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        popup.menuInflater.inflate(R.menu.card_study, popup.menu)
        popup.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.info -> {
                    val p1: Int = entity.progress
                    val p2: Int = (entity.progress.minus(1) / 5f * 100).toInt()
                    val p3: String = entity.tags.joinToString(", ")
                    val msg = context.getString(R.string.card_info, p1, p2, p3)
                    MaterialAlertDialogBuilder(context)
                        .setMessage(msg)
                        .setNeutralButton(R.string.ok) { _, _ -> }
                        .show()
                }
                R.id.edit -> onEditCard?.invoke(entity)
                R.id.delete -> onDeleteCard?.invoke(entity)
            }
            return@setOnMenuItemClickListener true
        }
        popup.show()
    }
}