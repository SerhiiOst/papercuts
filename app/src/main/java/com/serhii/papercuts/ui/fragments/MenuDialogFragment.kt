package com.serhii.papercuts.ui.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDelegate
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.firebase.auth.FirebaseAuth
import com.serhii.papercuts.R
import com.serhii.papercuts.managers.SharedPreferencesManager
import com.serhii.papercuts.ui.activities.ArchiveActivity
import com.serhii.papercuts.ui.activities.AuthActivity
import com.serhii.papercuts.ui.activities.ExploreActivity
import com.serhii.papercuts.ui.activities.ProgressActivity
import kotlinx.android.synthetic.main.dialog_menu.*
import org.koin.android.ext.android.inject

class MenuDialogFragment : BottomSheetDialogFragment() {

    private val sharedPreferencesManager: SharedPreferencesManager by inject()

    private val auth = FirebaseAuth.getInstance()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.dialog_menu, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()
    }

    private fun setupViews() {
        auth.currentUser?.let {
            mail.text = it.email
        }
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_NO)
            themeBtn.setImageResource(R.drawable.ic_night)
        themeBtn.setOnClickListener {
            sharedPreferencesManager.changeTheme()
        }
        exploreBtn.setOnClickListener {
            goToExplore()
            dismiss()
        }
        progressBtn.setOnClickListener {
            goToProgress()
            dismiss()
        }
        archiveBtn.setOnClickListener {
            goToArchive()
            dismiss()
        }
        signOutBtn.setOnClickListener {
            signOut()
        }
    }

    private fun goToProgress() {
        activity?.startActivity(Intent(requireActivity(), ProgressActivity::class.java))
    }

    private fun goToExplore() {
        activity?.startActivity(Intent(requireActivity(), ExploreActivity::class.java))
    }

    private fun goToArchive() {
        activity?.startActivity(Intent(requireActivity(), ArchiveActivity::class.java))
    }

    private fun signOut() {
        auth.signOut()
        context?.startActivity(Intent(context, AuthActivity::class.java))
        activity?.finish()
    }
}