package com.serhii.papercuts.ui.activities

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.serhii.papercuts.R
import com.serhii.papercuts.ui.adapters.recycler.CardAdapter
import com.serhii.papercuts.utils.view.BaseActivity
import com.serhii.papercuts.viewmodel.ArchiveViewModel
import kotlinx.android.synthetic.main.activity_archive.*
import kotlinx.android.synthetic.main.activity_archive.backBtn
import kotlinx.android.synthetic.main.activity_archive.cardsRV
import kotlinx.android.synthetic.main.activity_archive.swipeRefresh
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel

class ArchiveActivity: BaseActivity() {

    private val viewModel: ArchiveViewModel by viewModel()

    private val cardAdapter: CardAdapter by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        upgradeStatusBar()
        setContentView(R.layout.activity_archive)

        window.statusBarColor = ContextCompat.getColor(this, R.color.background_secondary)

        setupViews()
        setupTagsRV()
        setupCardRV()
        setupListeners()
    }

    private fun setupViews() {
        backBtn.setOnClickListener { onBackPressed() }
        swipeRefresh.setOnRefreshListener {
            refresh()
        }
        reviseBtn.setOnClickListener { goToStudy() }
    }

    private fun setupCardRV() {
        cardsRV.layoutManager =
            StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL)

        cardsRV.adapter = cardAdapter
    }

    private fun setupTagsRV() {

    }

    private fun setupListeners() {
        viewModel.cardsLiveData.observe(this, {
            swipeRefresh.isRefreshing = false
            cardAdapter.items = it
        })
    }

    private fun goToStudy() {
        startActivity(Intent(this, StudyActivity::class.java).also {
            it.putExtra(StudyActivity.ARCHIVE_KEY, true)
            it.putExtra(
                StudyActivity.TAGS_KEY,
                viewModel.searchTags.joinToString(", ") { t -> t })
        })
    }

    private fun refresh() {
        viewModel.getArchivedCards()
    }
}