package com.serhii.papercuts.ui.fragments

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.serhii.papercuts.R
import com.serhii.papercuts.model.entities.DeckEntity
import com.serhii.papercuts.ui.activities.DeckActivity
import com.serhii.papercuts.ui.adapters.recycler.DeckAdapter
import com.serhii.papercuts.utils.view.BaseFragment
import com.serhii.papercuts.viewmodel.LibraryViewModel
import kotlinx.android.synthetic.main.fragment_library_decks.*
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.sharedViewModel


class DeckLibraryFragment : BaseFragment() {

    companion object {
        private const val DECK_INFO_CODE = 1
    }

    private val activityVM: LibraryViewModel by sharedViewModel()

    private val deckAdapter: DeckAdapter by inject()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(R.layout.fragment_library_decks, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupViews()
        setupRV()
        setupListeners()
    }

    private fun setupViews() {
        swipeRefresh.setOnRefreshListener { activityVM.getDecks() }
    }

    private fun setupRV() {
        decksRV.layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
        decksRV.adapter = deckAdapter

        deckAdapter.onClickFun =
            {
                val i = Intent(context, DeckActivity::class.java).also { i ->
                    i.putExtra(DeckActivity.DECK_KEY, it)
                }
                startActivityForResult(i, DECK_INFO_CODE)
            }
    }

    private fun setupListeners() {
        activityVM.decksLiveData.observe(viewLifecycleOwner, {
            deckAdapter.items = it
            swipeRefresh.isRefreshing = false
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK)
            when (requestCode) {
                DECK_INFO_CODE -> {
                    data?.getStringExtra(DeckActivity.DECK_KEY)?.let {
                            deckAdapter.items = deckAdapter.items?.filterNot { d -> d.id == it }
                    }
                }
            }
    }
}