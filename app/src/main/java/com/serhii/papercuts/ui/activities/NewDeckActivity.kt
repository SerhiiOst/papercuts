package com.serhii.papercuts.ui.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.core.content.ContextCompat
import com.serhii.papercuts.R
import com.serhii.papercuts.model.entities.DeckEntity
import com.serhii.papercuts.utils.view.BaseActivity
import com.serhii.papercuts.viewmodel.CreateDeckViewModel
import kotlinx.android.synthetic.main.activity_create_deck.*
import kotlinx.android.synthetic.main.item_deck.view.*
import org.koin.android.viewmodel.ext.android.viewModel

class NewDeckActivity : BaseActivity() {

    companion object {
        const val DECK_KEY = "deck"
        const val DECK_NAME_KEY = "name"
        const val DECK_DESCRIPTION_KEY = "description"
    }

    private val viewModel: CreateDeckViewModel by viewModel()

    private var isEditMode = false
    private var deckId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_deck)
        upgradeStatusBar()

        window.statusBarColor = ContextCompat.getColor(this, R.color.background_secondary)

        setExtras()
        setupViews()
        setupListeners()
    }

    private fun setExtras() {
        intent.getSerializableExtra(DECK_KEY)?.let {
            if(it is DeckEntity) {
                isEditMode = true
                deckId = it.id
                nameEdit.setText(it.name)
                descriptionEdit.setText(it.description)

                createBtn.text = getString(R.string.update)
            }
        }
    }

    private fun setupViews() {
        backBtn.setOnClickListener { onBackPressed() }

        nameEdit.requestFocus()

        createBtn.setOnClickListener {
            if(isEditMode) {
                viewModel.updateDeck(
                    deckId,
                    nameEdit.text.toString(),
                    descriptionEdit.text.toString()
                )
            } else {
                viewModel.createDeck(
                    nameEdit.text.toString(),
                    descriptionEdit.text.toString()
                )
            }
        }
    }

    private fun setupListeners() {
        viewModel.deckCreated.observe(this, {
            if(it) {
                setResult(Activity.RESULT_OK)
                finish()
            }
        })
        viewModel.deckUpdated.observe(this, {
            if(it) {
                val i = Intent().also { i ->
                    i.putExtra(DECK_NAME_KEY, nameEdit.text.toString())
                    i.putExtra(DECK_DESCRIPTION_KEY, descriptionEdit.text.toString())
                }
                setResult(Activity.RESULT_OK, i)
                finish()
            }
        })
    }
}