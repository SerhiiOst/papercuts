package com.serhii.papercuts.ui.adapters.recycler

import android.content.Context
import android.view.Gravity
import android.view.View
import android.widget.PopupMenu
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.serhii.papercuts.R
import com.serhii.papercuts.model.entities.CardEntity
import com.serhii.papercuts.utils.BaseAdapter
import com.serhii.papercuts.utils.extencions.setupIcons
import com.serhii.papercuts.utils.view.FlashCardView

class DeckCardAdapter : BaseAdapter<CardEntity>() {

    override val layoutId = R.layout.item_card

    var onSelectCard: ((CardEntity) -> Unit)? = null
    var onEditCard: ((CardEntity) -> Unit)? = null
    var onAddToDeck: ((CardEntity) -> Unit)? = null
    var onRemoveCard: ((CardEntity) -> Unit)? = null
    var onDeleteCard: ((CardEntity) -> Unit)? = null

    var onSelectedCardsSizeChanged: ((Int) -> Unit)? = null

    val selectedCards = ArrayList<CardEntity>()
    private var isSelectMode = false

    override fun bind(itemView: View, entity: CardEntity, position: Int) {
        if (itemView is FlashCardView)
            with(itemView) {
                cardEntity = entity
                toggleSelect(selectedCards.contains(entity))
                onClick = {
                    if (isSelectMode) {
                        if (selectedCards.contains(entity)) {
                            toggleSelect(false)
                            selectedCards.remove(entity)
                            onSelectedCardsSizeChanged?.invoke(selectedCards.size)
                            isSelectMode = selectedCards.isNotEmpty()
                        } else {
                            toggleSelect(true)
                            selectedCards.add(entity)
                            onSelectedCardsSizeChanged?.invoke(selectedCards.size)
                        }
                    } else
                        flipFun.invoke()
                }
                onLongClick = {
                    if (isSelectMode)
                        flipFun.invoke()
                    else showContextMenu(context, this, entity)
                    true
                }
            }
    }

    private fun showContextMenu(context: Context, anchor: FlashCardView, entity: CardEntity) {
        val popup = PopupMenu(context, anchor, Gravity.TOP)
        popup.setupIcons()
        popup.menuInflater.inflate(R.menu.card_in_deck, popup.menu)
        popup.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.info -> {
                    val p1: Int = entity.progress
                    val p2: Int = (entity.progress.minus(1) / 5f * 100).toInt()
                    val p3: String = entity.tags.joinToString(", ")
                    val msg = context.getString(R.string.card_info, p1, p2, p3)
                    MaterialAlertDialogBuilder(context)
                        .setMessage(msg)
                        .setNeutralButton(R.string.ok) { _, _ -> }
                        .show()
                }
                R.id.select -> {
                    isSelectMode = true
                    selectedCards.add(entity)
                    anchor.toggleSelect(true)
                    onSelectCard?.invoke(entity)
                    onSelectedCardsSizeChanged?.invoke(selectedCards.size)
                }
                R.id.add_to_deck -> onAddToDeck?.invoke(entity)
                R.id.edit -> onEditCard?.invoke(entity)
                R.id.remove -> onRemoveCard?.invoke(entity)
                R.id.delete -> onDeleteCard?.invoke(entity)
            }
            return@setOnMenuItemClickListener true
        }
        popup.show()
    }

    fun cancelSelectMode() {
        selectedCards.clear()
        isSelectMode = false
        notifyDataSetChanged()
    }
}