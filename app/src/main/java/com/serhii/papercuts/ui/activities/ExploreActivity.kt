package com.serhii.papercuts.ui.activities

import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.serhii.papercuts.R
import com.serhii.papercuts.ui.adapters.recycler.CardAdapter
import com.serhii.papercuts.ui.adapters.recycler.CardExploreAdapter
import com.serhii.papercuts.ui.adapters.recycler.TagAdapter
import com.serhii.papercuts.utils.view.BaseActivity
import com.serhii.papercuts.viewmodel.ExploreViewModel
import kotlinx.android.synthetic.main.activity_explore.backBtn
import kotlinx.android.synthetic.main.activity_explore.cardsRV
import kotlinx.android.synthetic.main.activity_explore.libraryTagsRV
import kotlinx.android.synthetic.main.activity_explore.searchTagsRV
import kotlinx.android.synthetic.main.activity_explore.swipeRefresh
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel

class ExploreActivity : BaseActivity() {

    private val viewModel: ExploreViewModel by viewModel()

    private val cardAdapter: CardExploreAdapter by inject()
    private val searchTagAdapter: TagAdapter by inject()
    private val libraryTagAdapter: TagAdapter by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_explore)
        upgradeStatusBar()

        window.statusBarColor = ContextCompat.getColor(this, R.color.background_secondary)

        setupViews()
        setupRV()
        setupListeners()
    }

    private fun setupViews() {
        swipeRefresh.setOnRefreshListener {
//            refresh()
        }
        backBtn.setOnClickListener { onBackPressed() }
    }

    private fun setupRV() {
        cardsRV.layoutManager =
            StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL)

        cardsRV.adapter = cardAdapter

        cardAdapter.onAddCard = {
            viewModel.likeCard(it)
        }

        searchTagsRV.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        libraryTagsRV.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)

        searchTagAdapter.isTagClickable = false
        searchTagAdapter.closeBtnEnabled = true
        searchTagAdapter.onCloseClickFun = {
            libraryTagAdapter.addTag(it)
            viewModel.searchTags.remove(it)
//            viewModel.getUserCards()
        }
        searchTagsRV.adapter = searchTagAdapter

        libraryTagAdapter.onItemClickFun = {
            searchTagAdapter.addTag(it)
            viewModel.searchTags.add(it)
//            viewModel.getUserCards()
        }
        libraryTagsRV.adapter = libraryTagAdapter
    }

    private fun setupListeners() {
        viewModel.tagsLiveData.observe(this, {
            libraryTagAdapter.items =
                it.filterNot { t -> searchTagAdapter.items?.contains(t) == true }
            libraryTagAdapter.notifyDataSetChanged()
        })
        viewModel.cardsLiveData.observe(this, {
            swipeRefresh.isRefreshing = false
            cardAdapter.items = it
        })
        viewModel.likedCardLiveData.observe(this, {
            it.isLiked = true
            cardAdapter.refreshCard(it)
        })
    }
}