package com.serhii.papercuts.ui.adapters.recycler

import android.content.Context
import android.view.Gravity
import android.view.View
import android.widget.PopupMenu
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.serhii.papercuts.R
import com.serhii.papercuts.model.entities.CardEntity
import com.serhii.papercuts.utils.BaseAdapter
import com.serhii.papercuts.utils.view.FlashCardView
import org.koin.ext.getOrCreateScope
import java.lang.reflect.Field
import java.lang.reflect.Method

class SelectCardAdapter : BaseAdapter<CardEntity>() {

    override val layoutId = R.layout.item_card

    var onSelectedCardsSizeChanged: ((Int) -> Unit)? = null

    val selectedCards = ArrayList<CardEntity>()

    override fun bind(itemView: View, entity: CardEntity, position: Int) {
        if (itemView is FlashCardView)
            with(itemView) {
                cardEntity = entity
                toggleSelect(selectedCards.contains(entity))
                onClick = {
                    if (selectedCards.contains(entity)) {
                        toggleSelect(false)
                        selectedCards.remove(entity)
                        onSelectedCardsSizeChanged?.invoke(selectedCards.size)
                    } else {
                        toggleSelect(true)
                        selectedCards.add(entity)
                        onSelectedCardsSizeChanged?.invoke(selectedCards.size)
                    }
                }
                onLongClick = {
                    flipFun.invoke()
                    true
                }
            }
    }
}