package com.serhii.papercuts.ui.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.serhii.papercuts.R
import com.serhii.papercuts.managers.SharedPreferencesManager
import com.serhii.papercuts.ui.activities.MainActivity
import com.serhii.papercuts.utils.extencions.showToast
import com.serhii.papercuts.utils.view.BaseActivity
import com.serhii.papercuts.viewmodel.AuthViewModel
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.compat.ScopeCompat.viewModel
import org.koin.android.viewmodel.ext.android.viewModel

class AuthActivity : BaseActivity() {

    companion object {
        private const val RC_SIGN_IN = 11
        private const val RC_FACEBOOK_SIGN_IN = 12
        private const val RC_GOOGLE_SIGN_IN = 13
    }

    private val viewModel: AuthViewModel by viewModel()
    private val sharedPreferencesManager: SharedPreferencesManager by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        upgradeStatusBar()
        setContentView(R.layout.activity_auth)

        window.statusBarColor = ContextCompat.getColor(this, R.color.background)

        if (viewModel.auth.currentUser != null)
            goToMain()

        setupListeners()
    }

    private fun setupListeners() {
        viewModel.signInResultLiveData.observe(this, {
            if (it)
                goToMain()
        })
    }

    fun signInWithGoogle() {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()

        val client = GoogleSignIn.getClient(this, gso)

        val signInIntent = client.signInIntent
        startActivityForResult(signInIntent, RC_GOOGLE_SIGN_IN)
    }

    private fun goToMain() {
        sharedPreferencesManager.userAccessToken = FirebaseAuth.getInstance().uid
//        sharedPreferencesManager.userAccessToken = "J2CvIpZpQQbMoEXhR0ZMilp9gRC2"
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }

    override fun onBackPressed() {
        if (!findNavController(R.id.authNavHost).navigateUp())
            super.onBackPressed()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK)
            when (requestCode) {
                RC_SIGN_IN -> {
                }
                RC_FACEBOOK_SIGN_IN -> {
                }
                RC_GOOGLE_SIGN_IN -> {
                    val task: Task<GoogleSignInAccount> =
                        GoogleSignIn.getSignedInAccountFromIntent(data)
                    val account = task.result
                    account?.idToken?.let {
                        viewModel.signInWithCred(it)
                    }
                }
            }
    }
}