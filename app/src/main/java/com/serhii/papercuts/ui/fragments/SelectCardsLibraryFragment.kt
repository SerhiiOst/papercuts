package com.serhii.papercuts.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.serhii.papercuts.R
import com.serhii.papercuts.ui.adapters.recycler.CardAdapter
import com.serhii.papercuts.ui.adapters.recycler.SelectCardAdapter
import com.serhii.papercuts.ui.adapters.recycler.TagAdapter
import com.serhii.papercuts.utils.view.BaseFragment
import com.serhii.papercuts.viewmodel.SelectCardsViewModel
import kotlinx.android.synthetic.main.fragment_library_cards.*
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.sharedViewModel

class SelectCardsLibraryFragment : BaseFragment() {

    private val activityVM: SelectCardsViewModel by sharedViewModel()

    private val searchTagAdapter: TagAdapter by inject()
    private val libraryTagAdapter: TagAdapter by inject()
    private val cardAdapter: SelectCardAdapter by inject()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(R.layout.fragment_library_cards, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupViews()
        setupTagsRV()
        setupCardRV()
        setupListeners()
    }

    private fun setupViews() {
        backBtn.setOnClickListener { activity?.onBackPressed() }
        swipeRefresh.setOnRefreshListener {
            refresh()
        }
    }

    private fun setupTagsRV() {
        searchTagsRV.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        libraryTagsRV.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)

        searchTagAdapter.isTagClickable = false
        searchTagAdapter.closeBtnEnabled = true
        searchTagAdapter.onCloseClickFun = {
            libraryTagAdapter.addTag(it)
            activityVM.searchTags.remove(it)
            activityVM.getUserCards()
        }
        searchTagsRV.adapter = searchTagAdapter

        libraryTagAdapter.onItemClickFun = {
            searchTagAdapter.addTag(it)
            activityVM.searchTags.add(it)
            activityVM.getUserCards()
        }
        libraryTagsRV.adapter = libraryTagAdapter
    }

    private fun setupCardRV() {
        cardsRV.layoutManager =
            StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL)

        cardAdapter.onSelectedCardsSizeChanged = {
            activityVM.selectedUserCardsLiveData.value = cardAdapter.selectedCards
        }

        cardsRV.adapter = cardAdapter
    }

    private fun setupListeners() {
        activityVM.tagsLiveData.observe(viewLifecycleOwner, {
            libraryTagAdapter.items =
                it.filterNot { t -> searchTagAdapter.items?.contains(t) == true }
            libraryTagAdapter.notifyDataSetChanged()
        })
        activityVM.cardsLiveData.observe(viewLifecycleOwner, {
            swipeRefresh.isRefreshing = false
            cardAdapter.items = it
        })
    }

    override fun refresh() {
        activityVM.getUserCards()
        activityVM.getUserTags()
    }
}