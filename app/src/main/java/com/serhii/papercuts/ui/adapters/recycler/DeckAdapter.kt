package com.serhii.papercuts.ui.adapters.recycler

import android.view.View
import androidx.core.view.isVisible
import com.serhii.papercuts.R
import com.serhii.papercuts.model.entities.DeckEntity
import com.serhii.papercuts.utils.BaseAdapter
import kotlinx.android.synthetic.main.item_deck.view.*

class DeckAdapter : BaseAdapter<DeckEntity>() {

    override val layoutId = R.layout.item_deck

    var onClickFun: ((DeckEntity) -> Unit)? = null

    override fun bind(itemView: View, entity: DeckEntity, position: Int) {
        with(itemView) {
            name.text = entity.name
            description.text = entity.description
            description.isVisible = entity.description.isNotEmpty()
            setOnClickListener { onClickFun?.invoke(entity) }
        }
    }
}