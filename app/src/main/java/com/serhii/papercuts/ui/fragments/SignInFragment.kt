package com.serhii.papercuts.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.serhii.papercuts.R
import com.serhii.papercuts.ui.activities.AuthActivity
import com.serhii.papercuts.utils.extencions.getActivity
import com.serhii.papercuts.utils.view.BaseFragment
import com.serhii.papercuts.viewmodel.AuthViewModel
import kotlinx.android.synthetic.main.fragment_sign_in.*
import org.koin.android.viewmodel.ext.android.sharedViewModel

class SignInFragment : BaseFragment() {

    private val activityVM: AuthViewModel by sharedViewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(R.layout.fragment_sign_in, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupViews()
        setupListeners()
    }

    private fun setupViews() {
        signInBtn.setOnClickListener {
            activityVM.signInWithEmail(
                emailEdit.text.toString(),
                passwordEdit.text.toString()
            )
        }
        signInWithFacebookBtn.setOnClickListener { }
        signInWithGoogleBtn.setOnClickListener {
            getActivity<AuthActivity>()?.signInWithGoogle()
        }

        signUpBtn.setOnClickListener {
            findNavController().navigate(R.id.action_signInFragment_to_signUpFragment)
        }
    }

    private fun setupListeners() {
        activityVM.signInErrorLiveData.observe(viewLifecycleOwner, Observer {
            errorText.text = it
            errorText.visibility = View.VISIBLE
        })
    }
}