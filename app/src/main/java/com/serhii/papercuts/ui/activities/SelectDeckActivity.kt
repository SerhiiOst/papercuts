package com.serhii.papercuts.ui.activities

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.serhii.papercuts.R
import com.serhii.papercuts.ui.adapters.recycler.DeckAdapter
import com.serhii.papercuts.utils.view.BaseActivity
import com.serhii.papercuts.viewmodel.AddToDeckViewModel
import kotlinx.android.synthetic.main.activity_select_deck.*
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel

class SelectDeckActivity : BaseActivity() {

    companion object {
        private const val CODE_CREATE_DECK = 1

        const val CARD_IDS_KEY = "cardIds"
    }

    private val viewModel: AddToDeckViewModel by viewModel()

    private val deckAdapter: DeckAdapter by inject()

    private var ids: List<String>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        upgradeStatusBar()
        setContentView(R.layout.activity_select_deck)

        window.statusBarColor = ContextCompat.getColor(this, R.color.background_secondary)

        ids = intent.getStringArrayListExtra(CARD_IDS_KEY)

        setupViews()
        setupRV()
        setupListeners()
    }

    private fun setupViews() {
        backBtn.setOnClickListener { onBackPressed() }

        createBtn.setOnClickListener {
            startActivityForResult(Intent(this, NewDeckActivity::class.java), CODE_CREATE_DECK)
        }
    }

    private fun setupRV() {
        decksRV.layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
        decksRV.adapter = deckAdapter

        deckAdapter.onClickFun = {
            MaterialAlertDialogBuilder(this)
                .setTitle(R.string.add_to_deck)
                .setMessage(getString(R.string.you_sure_add_cards_to_deck, ids?.size ?: 0))
                .setNegativeButton(R.string.cancel) {_, _ ->}
                .setPositiveButton(R.string.ok) {_, _ ->
                    viewModel.addCardsToDeck(it.id, ids ?: emptyList())
                }
                .show()
        }
    }

    private fun setupListeners() {
        viewModel.decksLiveData.observe(this, {
            deckAdapter.items = it
        })
        viewModel.successLiveData.observe(this, {
            if(it)
                finish()
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(resultCode == Activity.RESULT_OK)
            when(requestCode) {
                CODE_CREATE_DECK -> viewModel.getDecks()
            }
    }
}