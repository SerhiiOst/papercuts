package com.serhii.papercuts.ui.activities

import android.graphics.Color
import android.os.Bundle
import androidx.core.content.ContextCompat
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.utils.ColorTemplate
import com.serhii.papercuts.R
import com.serhii.papercuts.utils.view.BaseActivity
import com.serhii.papercuts.viewmodel.ProgressViewModel
import kotlinx.android.synthetic.main.activity_progress.*
import org.koin.android.viewmodel.ext.android.viewModel

class ProgressActivity : BaseActivity() {

    companion object {
        const val PROGRESS_ARRAY_KEY = "progress"
        const val TAGS_KEY = "tags"
    }

    private val viewModel: ProgressViewModel by viewModel()

    private var query = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_progress)

        upgradeStatusBar()

        setupViews()
        setupListeners()

        val list = intent.getIntegerArrayListExtra(PROGRESS_ARRAY_KEY)
        query = intent.getStringExtra(TAGS_KEY) ?: ""
        if (list.isNullOrEmpty())
            viewModel.getProgress(query)
        else
            viewModel.progressLiveData.postValue(list)
    }

    private fun setupViews() {
        backBtn.setOnClickListener { onBackPressed() }

        chart.setCenterTextColor(ContextCompat.getColor(this, R.color.dark_light))
        chart.setHoleColor(Color.TRANSPARENT)
        chart.setCenterTextSize(16f)
        chart.setNoDataText("")
        chart.setDrawEntryLabels(false)
        chart.description.isEnabled = false
        chart.setUsePercentValues(true)
        chart.isClickable = false

//            chart.legend.isEnabled = false
        chart.legend.textColor = ContextCompat.getColor(this, R.color.dark_light)
        chart.legend.textSize = 14f
        chart.legend.formToTextSpace = 5f
        chart.legend.xEntrySpace = 25f
    }

    private fun setupListeners() {
        viewModel.progressLiveData.observe(this, {
            val entries = ArrayList<PieEntry>()
            val percent = calculatePercent(it).toInt()
            it.distinct().sorted().forEach { value ->
                entries.add(
                    PieEntry(
                        it.filter { p -> p == value }.size.toFloat(),
                        value.toString()
                    )
                )
            }
            val pieDataSet = PieDataSet(entries, "")
            pieDataSet.colors = resources.getIntArray(R.array.pie_chart).toList()
            pieDataSet.valueTextColor = Color.WHITE
            pieDataSet.valueTextSize = 12f

            chart.data = PieData(pieDataSet)
            val str =
                if (query.isEmpty()) R.string.progress_of_all else R.string.progress_of_selected
            chart.centerText = getString(str, percent)
            chart.invalidate()
            chart.animate()
        })
    }

    private fun calculatePercent(list: List<Int>): Float {
        val sum = list.sum().toFloat() - list.size
        val sumMax = list.size * 5f
        return (sum / sumMax) * 100
    }
}