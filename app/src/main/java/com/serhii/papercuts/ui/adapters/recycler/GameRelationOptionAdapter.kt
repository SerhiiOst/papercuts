package com.serhii.papercuts.ui.adapters.recycler

import android.graphics.Color
import android.os.Handler
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import com.google.android.material.card.MaterialCardView
import com.serhii.papercuts.R
import com.serhii.papercuts.model.entities.DeckEntity
import com.serhii.papercuts.model.entities.GameRelationOptionEntity
import com.serhii.papercuts.model.entities.RelationOptionState
import com.serhii.papercuts.utils.BaseAdapter
import kotlinx.android.synthetic.main.item_relation_option.view.*

class GameRelationOptionAdapter : BaseAdapter<GameRelationOptionEntity>() {

    override val layoutId = R.layout.item_relation_option

    var onClickFun: ((GameRelationOptionEntity) -> Unit)? = null

    var selectedEntity: GameRelationOptionEntity? = null

    override fun bind(itemView: View, entity: GameRelationOptionEntity, position: Int) {
        if (itemView is MaterialCardView)
            with(itemView) {
                optionText.text = entity.value.replace("\\n", "\n")
                setOnClickListener {
                    selectedEntity?.state = RelationOptionState.IDLE
                    notifySelectedItem()
                    selectedEntity = entity
                    onClickFun?.invoke(entity)
                }
                isVisible = entity.state != RelationOptionState.ANSWERED
                val colorRes =
                    when (entity.state) {
                        RelationOptionState.SELECTED -> R.color.card_background_selected
                        RelationOptionState.WRONG -> R.color.red
                        RelationOptionState.RIGHT -> R.color.green
                        else -> R.color.card_background
                    }
                val textColorRes =
                    if (entity.state == RelationOptionState.WRONG || entity.state == RelationOptionState.RIGHT)
                        R.color.text_color_highlighted
                    else
                        R.color.text_color
                setCardBackgroundColor(ContextCompat.getColor(context, colorRes))
                optionText.setTextColor(ContextCompat.getColor(context, textColorRes))

                if (entity.state == RelationOptionState.WRONG) {
                    isClickable = false
                    Handler().postDelayed({
                        isClickable = true
                        entity.state = RelationOptionState.IDLE
                        notifyItemChanged(position)
                    }, 500)
                }
                if (entity.state == RelationOptionState.RIGHT) {
                    isClickable = false
                    Handler().postDelayed({
                        entity.state = RelationOptionState.ANSWERED
                        notifyItemChanged(position)
                    }, 500)
                }
            }
    }

    fun notifySelectedItem() {
        items?.indexOf(selectedEntity)?.let {
            notifyItemChanged(it)
        }
    }
}