package com.serhii.papercuts.ui.activities

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.jakewharton.rxbinding4.widget.textChanges
import com.serhii.papercuts.R
import com.serhii.papercuts.managers.SharedPreferencesManager
import com.serhii.papercuts.model.entities.CardEntity
import com.serhii.papercuts.ui.fragments.CardLibraryFragment
import com.serhii.papercuts.ui.fragments.DeckLibraryFragment
import com.serhii.papercuts.ui.fragments.MenuDialogFragment
import com.serhii.papercuts.utils.extencions.addItems
import com.serhii.papercuts.utils.extencions.removeItems
import com.serhii.papercuts.utils.view.BaseActivity
import com.serhii.papercuts.viewmodel.LibraryViewModel
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import java.util.concurrent.TimeUnit

class MainActivity : BaseActivity() {

    private val viewModel: LibraryViewModel by viewModel()

    private val sharedPreferencesManager: SharedPreferencesManager by inject()

    companion object {
        private const val CODE_CREATE_CARD = 1
        private const val CODE_CREATE_DECK = 2
        private const val CODE_UPDATE_CARDS = 3
        private const val CODE_UPDATE_DECKS = 4

        private const val KEY_ADD_CARDS = "add_cards"
        private const val KEY_REMOVE_CARDS = "remove_cards"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        upgradeStatusBar()
        AppCompatDelegate.setDefaultNightMode(sharedPreferencesManager.appTheme)
        setContentView(R.layout.activity_main)

        window.statusBarColor = ContextCompat.getColor(this, R.color.background_secondary)

        setupViews()
        setupListeners()
    }

    private fun setupViews() {
        //Pager
        viewPager.adapter = LibraryPagerAdapter(
            this,
            supportFragmentManager,
            FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT
        )
        libraryTabs.setupWithViewPager(viewPager)
        viewPager.addOnPageChangeListener(object: ViewPager.OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {}

            override fun onPageSelected(position: Int) {
                val isCards = position == 0
//                createCardFab.isVisible = isCards
//                createDeckFab.isVisible = !isCards

                if(isCards) {
                    createCardFab.show()
                    createDeckFab.hide()
                } else {
                    createCardFab.hide()
                    createDeckFab.show()
                }
                searchBtn.isVisible = isCards
            }

            override fun onPageScrollStateChanged(state: Int) {}
        })

        //Fabs
        createCardFab.setOnClickListener { goToCreateCard() }
        createDeckFab.setOnClickListener { goToCreateDeck() }
        studyFab.setOnClickListener { goToStudy() }

        //BottomAppBar
        bottomAppBar.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.explore -> goToExplore()
                R.id.progress -> goToProgress()
            }
            return@setOnMenuItemClickListener true
        }
        bottomAppBar.setNavigationOnClickListener {
            MenuDialogFragment().show(supportFragmentManager, "")
        }

        //ActionBar
        searchEdit.textChanges()
            .debounce(500, TimeUnit.MILLISECONDS)
            .subscribe {
                if (viewModel.searchQuery == it.toString())
                    return@subscribe
                viewModel.searchQuery = it.toString()
                viewModel.getUserCards()
            }
    }

    private fun setupListeners() {
        viewModel.selectModeLiveData.observe(this, {
            toggleSelectMode(it)
        })
    }

    private fun goToCreateCard() {
        startActivityForResult(
            Intent(
                this,
                NewCardActivity::class.java
            ).also {
                it.putExtra(
                    "tags",
                    viewModel.searchTags
                )
            },
            CODE_CREATE_CARD
        )
    }

    private fun goToCreateDeck() {
        startActivityForResult(
            Intent(
                this,
                NewDeckActivity::class.java
            ),
            CODE_CREATE_DECK
        )
    }

    private fun goToStudy() {
        startActivity(Intent(this, StudyActivity::class.java).also {
            it.putExtra(
                "tags",
                viewModel.searchTags.joinToString(", ") { t -> t })
        })
    }

    private fun goToProgress() {
        startActivity(Intent(this, ProgressActivity::class.java).also {
            it.putExtra(
                "tags",
                viewModel.searchTags.joinToString(", ") { t -> t })
        })
    }

    private fun goToExplore() {
        startActivity(Intent(this, ExploreActivity::class.java))
    }

    private fun toggleSelectMode(value: Boolean) {
        mainActionBar.isVisible = !value
        if (value) {
//            bottomAppBar.replaceMenu(R.menu.select)
            bottomAppBar.navigationIcon = null
            bottomAppBar.performHide()
            studyFab.hide()
            createCardFab.hide()
        } else {
//            bottomAppBar.replaceMenu(R.menu.main)
            bottomAppBar.setNavigationIcon(R.drawable.ic_menu)
            bottomAppBar.performShow()
            studyFab.show()
            createCardFab.show()
        }
    }

    override fun onBackPressed() {
        if (viewModel.selectModeLiveData.value == true)
            viewModel.selectModeLiveData.value = false
        else
            super.onBackPressed()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK)
            when (requestCode) {
                CODE_CREATE_CARD -> {
                    viewModel.getUserTags()
                    viewModel.getUserCards()
                }
                CODE_CREATE_DECK -> {
                    viewModel.getDecks()
                }
            }
    }

    class LibraryPagerAdapter(
        private val context: Context,
        fm: FragmentManager, state: Int
    ) : FragmentStatePagerAdapter(fm, state) {

        private val fragments = listOf(
            CardLibraryFragment(),
            DeckLibraryFragment()
        )

        private val titles = listOf(
            R.string.cards,
            R.string.decks
        )

        override fun getPageTitle(position: Int) = context.getString(titles[position])

        override fun getCount() = fragments.size

        override fun getItem(position: Int) = fragments[position]
    }
}