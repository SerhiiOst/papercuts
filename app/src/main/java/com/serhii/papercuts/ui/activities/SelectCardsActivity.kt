package com.serhii.papercuts.ui.activities

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.ViewPager
import com.jakewharton.rxbinding4.widget.textChanges
import com.serhii.papercuts.R
import com.serhii.papercuts.ui.fragments.SelectCardsLibraryFragment
import com.serhii.papercuts.ui.fragments.DeckLibraryFragment
import com.serhii.papercuts.utils.view.BaseActivity
import com.serhii.papercuts.viewmodel.AddToDeckViewModel
import com.serhii.papercuts.viewmodel.SelectCardsViewModel
import kotlinx.android.synthetic.main.activity_select_cards.*
import org.koin.android.viewmodel.ext.android.viewModel
import java.util.concurrent.TimeUnit

class SelectCardsActivity : BaseActivity() {

    private val viewModel: SelectCardsViewModel by viewModel()

    private val addToDeckViewModel: AddToDeckViewModel by viewModel()

    private var deckId = ""

    companion object {
        private const val CODE_CREATE_CARD = 1

        const val DECK_ID_KEY = "deckId"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        upgradeStatusBar()
        setContentView(R.layout.activity_select_cards)

        window.statusBarColor = ContextCompat.getColor(this, R.color.background_secondary)

        deckId = intent.getStringExtra(DECK_ID_KEY) ?: ""

        setupViews()
        setupListeners()
    }

    private fun setupViews() {
        backBtn.setOnClickListener { onBackPressed() }

        //Pager
        viewPager.adapter = PagerAdapter(
            this,
            supportFragmentManager,
            FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT
        )
        libraryTabs.setupWithViewPager(viewPager)
        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                //TODO
            }

            override fun onPageScrollStateChanged(state: Int) {}
        })

        //Fabs TODO
        createCardFab.setOnClickListener { goToCreateCard() }
        addBtn.setOnClickListener {
            viewModel.addCardsToDeck(deckId)
            it.isEnabled = false
        }

        //ActionBar
        searchEdit.textChanges()
            .debounce(500, TimeUnit.MILLISECONDS)
            .subscribe {
                if (viewModel.searchQuery == it.toString())
                    return@subscribe
                viewModel.searchQuery = it.toString()
                viewModel.getUserCards()
            }
    }

    private fun setupListeners() {
        viewModel.selectedUserCardsLiveData.observe(this, {
            addBtn.isEnabled = it.isNotEmpty()
        })
        viewModel.successLiveData.observe(this, {
            if (it) {
                setResult(Activity.RESULT_OK)
                finish()
            } else
                addBtn.isEnabled = true
        })
    }

    private fun goToCreateCard() {
        startActivityForResult(
            Intent(
                this,
                NewCardActivity::class.java
            ).also {
                it.putExtra(
                    "tags",
                    viewModel.searchTags
                )
            },
            CODE_CREATE_CARD
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK)
            when (requestCode) {
                CODE_CREATE_CARD -> {
//                    viewModel.getUserTags()
//                    viewModel.getUserCards()
                }
            }
    }

    class PagerAdapter(
        private val context: Context,
        fm: FragmentManager, state: Int
    ) : FragmentStatePagerAdapter(fm, state) {

        private val fragments = listOf(
            SelectCardsLibraryFragment(),
            DeckLibraryFragment()
        )

        private val titles = listOf(
            R.string.my_cards,
            R.string.library
        )

        override fun getPageTitle(position: Int) = context.getString(titles[position])

        override fun getCount() = fragments.size

        override fun getItem(position: Int) = fragments[position]
    }
}