package com.serhii.papercuts.ui.adapters.recycler

import android.view.View
import com.google.android.material.chip.Chip
import com.serhii.papercuts.R
import com.serhii.papercuts.model.entities.TagEntity
import com.serhii.papercuts.utils.BaseAdapter

class TagAdapter : BaseAdapter<String>() {

    override val layoutId = R.layout.item_tag
    override val notifyAutomatically = false

    var onItemClickFun: ((String) -> Unit)? = null
    var onCloseClickFun: ((String) -> Unit)? = null

    var closeBtnEnabled = false
    var isTagClickable = true

    fun addTag(tag: String) {
        if(items?.contains(tag) != true) {
            if(items == null)
                items = emptyList()
            val arr = ArrayList<String>()
            arr.add(tag)
            arr.addAll(items!!.toMutableList())
            items = arr.toList()
            notifyItemInserted(0)
        }
    }

    override fun bind(itemView: View, entity: String, position: Int) {
        with(itemView as Chip) {
            text = entity
            isCloseIconVisible = closeBtnEnabled
            setOnCloseIconClickListener {
                val pos = items?.indexOf(entity) ?: 0
                items = items?.filterNot { tag -> tag == entity }
                onCloseClickFun?.invoke(entity)
                notifyItemRemoved(pos)
            }
            if (isTagClickable)
                setOnClickListener {
                    val pos = items?.indexOf(entity) ?: 0
                    items = items?.filterNot { tag -> tag == entity }
                    onItemClickFun?.invoke(entity)
                    notifyItemRemoved(pos)
                }
        }
    }
}