package com.serhii.papercuts.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.serhii.papercuts.managers.ConnectionManager
import com.serhii.papercuts.managers.SharedPreferencesManager
import com.serhii.papercuts.model.network.ApiRequestService

class AuthViewModel(
    var apiRequestService: ApiRequestService,
    var sharedPreferencesManager: SharedPreferencesManager,
    var connectionDetector: ConnectionManager
) : ViewModel() {

    val auth = FirebaseAuth.getInstance()

    val signInResultLiveData = MutableLiveData<Boolean>()
    val signInErrorLiveData = MutableLiveData<String>()
    val signUpErrorLiveData = MutableLiveData<String>()

    fun signInWithCred(idToken: String) {
        val cred = GoogleAuthProvider.getCredential(idToken, null)
        auth.signInWithCredential(cred)
            .addOnCompleteListener { task ->
                if(task.isSuccessful) {
                    signInResultLiveData.postValue(true)
                } else {
                    signInErrorLiveData.postValue(task.exception?.message)
                }
            }
    }

    fun signInWithEmail(email: String, pass: String) {
        if(email.isEmpty() || pass.isEmpty()) {
            signInErrorLiveData.postValue("Please enter the fields")
            return
        }
        auth.signInWithEmailAndPassword(email, pass)
            .addOnCompleteListener { task ->
            if(task.isSuccessful) {
                signInResultLiveData.postValue(true)
            } else {
                signInErrorLiveData.postValue(task.exception?.message)
            }
        }
    }

    fun signUpWithEmail(email: String, pass: String) {
        if(email.isEmpty() || pass.isEmpty()) {
            signUpErrorLiveData.postValue("Please enter the fields")
            return
        }
        auth.createUserWithEmailAndPassword(email, pass)
            .addOnCompleteListener { task ->
                if(task.isSuccessful) {
//                    signInWithCred(auth.currentUser!!.getIdToken(true).toString())
                    signInResultLiveData.postValue(true)
                } else {
                    signUpErrorLiveData.postValue(task.exception?.message)
                }
            }
    }
}