package com.serhii.papercuts.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.serhii.papercuts.managers.ConnectionManager
import com.serhii.papercuts.managers.SharedPreferencesManager
import com.serhii.papercuts.model.entities.*
import com.serhii.papercuts.model.network.ApiRequestService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers

class CreateCardViewModel(
    var apiRequestService: ApiRequestService,
    var sharedPreferencesManager: SharedPreferencesManager,
    var connectionDetector: ConnectionManager
) : ViewModel() {

    private var compositeDisposable: CompositeDisposable = CompositeDisposable()
    val inProgress = MutableLiveData<Boolean>()
    val requestError = MutableLiveData<RequestError>()

    val uploadSuccess = MutableLiveData<Boolean>()
    val selectedDeckLiveData = MutableLiveData<DeckEntity>()
    val tagsLiveData = MutableLiveData<List<String>>()
    var frontIsImg = false
    var backIsImg = false

    fun addCard(front: String, back: String, isRandomSides: Boolean, tags: List<String>) {
        if (connectionDetector.isConnected.value != true) {
            requestError.postValue(RequestError(RequestError.CONNECTION_ERROR))
            return
        }
        inProgress.postValue(true)

        val body = CardRequest(
            if (frontIsImg) "" else front,
            if (frontIsImg) front else "",
            if (backIsImg) "" else back,
            if (backIsImg) back else "",
            isRandomSides,
            tags//,
//            selectedDeckLiveData.value?.id
        )

        compositeDisposable.add(
            apiRequestService
                .addCard(body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                    onSuccess = {
                        inProgress.postValue(false)
                        uploadSuccess.value = true
                    },
                    onError = {
                        inProgress.postValue(false)
                        if (connectionDetector.isConnected.value != true) {
                            requestError.postValue(RequestError(RequestError.CONNECTION_ERROR))
                        } else {
                            requestError.postValue(RequestError(RequestError.REQUEST_ERROR))
                        }
                    })
        )
    }

    fun addCards(cards: List<MiniCardEntity>, isRandomSides: Boolean, tags: List<String>) {
        if (connectionDetector.isConnected.value != true) {
            requestError.postValue(RequestError(RequestError.CONNECTION_ERROR))
            return
        }
        inProgress.postValue(true)

        val body = CardsPackEntity(cards, isRandomSides, tags)

        compositeDisposable.add(
            apiRequestService
                .addCardsPack(body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                    onSuccess = {
                        inProgress.postValue(false)
                        uploadSuccess.value = true
                    },
                    onError = {
                        inProgress.postValue(false)
                        if (connectionDetector.isConnected.value != true) {
                            requestError.postValue(RequestError(RequestError.CONNECTION_ERROR))
                        } else {
                            requestError.postValue(RequestError(RequestError.REQUEST_ERROR))
                        }
                    })
        )
    }

    fun updateCard(front: String, back: String, card: CardEntity, tags: List<String>) {
        if (connectionDetector.isConnected.value != true) {
            requestError.postValue(RequestError(RequestError.CONNECTION_ERROR))
            return
        }
        inProgress.postValue(true)

        val body = CardEntity(
            card.id,
            if (frontIsImg) "" else front,
            if (frontIsImg) front else "",
            if (backIsImg) "" else back,
            if (backIsImg) back else "",
            tags.distinct(),
            card.progress
        )

        compositeDisposable.add(
            apiRequestService
                .updateCard(card.id, body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                    onSuccess = {
                        inProgress.postValue(false)
                        uploadSuccess.value = true
                    },
                    onError = {
                        inProgress.postValue(false)
                        if (connectionDetector.isConnected.value != true) {
                            requestError.postValue(RequestError(RequestError.CONNECTION_ERROR))
                        } else {
                            requestError.postValue(RequestError(RequestError.REQUEST_ERROR))
                        }
                    })
        )
    }

    fun searchTags(query: String) {
        if (connectionDetector.isConnected.value != true) {
            requestError.postValue(RequestError(RequestError.CONNECTION_ERROR))
            return
        }
        inProgress.postValue(true)

        compositeDisposable.add(
            apiRequestService
                .searchTags(query)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                    onSuccess = {
                        inProgress.postValue(false)

                        it.body()?.let { body ->
                            tagsLiveData.value = body
                        }
                    },
                    onError = {
                        inProgress.postValue(false)
                        if (connectionDetector.isConnected.value != true) {
                            requestError.postValue(RequestError(RequestError.CONNECTION_ERROR))
                        } else {
                            requestError.postValue(RequestError(RequestError.REQUEST_ERROR))
                        }
                    })
        )
    }
}