package com.serhii.papercuts.viewmodel

import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.serhii.papercuts.managers.ConnectionManager
import com.serhii.papercuts.managers.SharedPreferencesManager
import com.serhii.papercuts.model.entities.*
import com.serhii.papercuts.model.network.ApiRequestService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers

class CreateDeckViewModel(
    var apiRequestService: ApiRequestService,
    var sharedPreferencesManager: SharedPreferencesManager,
    var connectionDetector: ConnectionManager
) : ViewModel() {

    private var compositeDisposable: CompositeDisposable = CompositeDisposable()
    var inProgress = MutableLiveData<Boolean>()
    val requestError = MutableLiveData<RequestError>()

    val deckCreated = MutableLiveData<Boolean>()
    val deckUpdated = MutableLiveData<Boolean>()
    val cardsLiveData = MutableLiveData<List<CardEntity>>()
    val tagsLiveData = MutableLiveData<List<TagEntity>>()

    val name = ObservableField("")
    val description = ObservableField("")

    fun createDeck(
        name: String,
        description: String
    ) {
        if (connectionDetector.isConnected.value != true) {
            requestError.postValue(RequestError(RequestError.CONNECTION_ERROR))
            return
        }
        inProgress.postValue(true)

        val body = DeckRequest(
            name,
            description
        )

        compositeDisposable.add(
            apiRequestService
                .addDeck(body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                    onSuccess = {
                        inProgress.postValue(false)

                        deckCreated.value = true
                    },
                    onError = {
                        inProgress.postValue(false)
                        if (connectionDetector.isConnected.value != true) {
                            requestError.postValue(RequestError(RequestError.CONNECTION_ERROR))
                        } else {
                            requestError.postValue(RequestError(RequestError.REQUEST_ERROR))
                        }
                    })
        )
    }

    fun updateDeck(id: String, name: String, description: String) {
        if (connectionDetector.isConnected.value != true) {
            requestError.postValue(RequestError(RequestError.CONNECTION_ERROR))
            return
        }
        inProgress.postValue(true)

        val body = DeckRequest(
            name,
            description
        )

        compositeDisposable.add(
            apiRequestService
                .updateDeck(id, body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                    onSuccess = {
                        inProgress.postValue(false)

                        deckUpdated.value = true
                    },
                    onError = {
                        inProgress.postValue(false)
                        if (connectionDetector.isConnected.value != true) {
                            requestError.postValue(RequestError(RequestError.CONNECTION_ERROR))
                        } else {
                            requestError.postValue(RequestError(RequestError.REQUEST_ERROR))
                        }
                    })
        )
    }
}