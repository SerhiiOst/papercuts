package com.serhii.papercuts.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.serhii.papercuts.managers.ConnectionManager
import com.serhii.papercuts.managers.SharedPreferencesManager
import com.serhii.papercuts.model.entities.*
import com.serhii.papercuts.model.network.ApiRequestService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers

class SelectCardsViewModel(
    var apiRequestService: ApiRequestService,
    var sharedPreferencesManager: SharedPreferencesManager,
    var connectionDetector: ConnectionManager
) : ViewModel() {

    private var compositeDisposable: CompositeDisposable = CompositeDisposable()
    var inProgress = MutableLiveData<Boolean>()
    val requestError = MutableLiveData<RequestError>()

    val cardsLiveData = MutableLiveData<List<CardEntity>>()
    val decksLiveData = MutableLiveData<List<DeckEntity>>()
    val tagsLiveData = MutableLiveData<List<String>>()
    val successLiveData = MutableLiveData<Boolean>()

    val selectedUserCardsLiveData = MutableLiveData<List<CardEntity>>()
    val selectedGlobalCardsLiveData = MutableLiveData<List<CardEntity>>()

    val searchTags = ArrayList<String>()
    var searchQuery = ""

    init {
        getUserTags()
        getUserCards()
    }

    fun getUserCards() {
        if (connectionDetector.isConnected.value != true) {
            requestError.postValue(RequestError(RequestError.CONNECTION_ERROR))
            return
        }
        inProgress.postValue(true)

        val request = if (searchTags.isEmpty())
            apiRequestService.getUserCards()
        else
            apiRequestService.searchUserCardsByTags(searchTags.joinToString(", ") { it })

        compositeDisposable.add(
            request
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                    onSuccess = {
                        inProgress.postValue(false)

                        cardsLiveData.value = it.body()
                    },
                    onError = {
                        inProgress.postValue(false)
                        if (connectionDetector.isConnected.value != true) {
                            requestError.postValue(RequestError(RequestError.CONNECTION_ERROR))
                        } else {
                            requestError.postValue(RequestError(RequestError.REQUEST_ERROR))
                        }
                    })
        )
    }

    fun getUserTags() {
        if (connectionDetector.isConnected.value != true) {
            requestError.postValue(RequestError(RequestError.CONNECTION_ERROR))
            return
        }
        inProgress.postValue(true)

        compositeDisposable.add(
            apiRequestService
                .getUserTags()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                    onSuccess = {
                        inProgress.postValue(false)
                        it.body()?.let { tags ->
                            tagsLiveData.value = tags
                        }
                    },
                    onError = {
                        inProgress.postValue(false)
                        if (connectionDetector.isConnected.value != true) {
                            requestError.postValue(RequestError(RequestError.CONNECTION_ERROR))
                        } else {
                            requestError.postValue(RequestError(RequestError.REQUEST_ERROR))
                        }
                    })
        )
    }

    fun addCardsToDeck(deckId: String) {
        if (connectionDetector.isConnected.value != true) {
            requestError.postValue(RequestError(RequestError.CONNECTION_ERROR))
            return
        }
        inProgress.postValue(true)

        val ids = selectedUserCardsLiveData.value?.map { it.id } ?: emptyList()

        compositeDisposable.add(
            apiRequestService
                .addCardsToDeck(deckId, ids)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                    onSuccess = {
                        inProgress.postValue(false)

                        successLiveData.value = true
                    },
                    onError = {
                        inProgress.postValue(false)
                        if (connectionDetector.isConnected.value != true) {
                            requestError.postValue(RequestError(RequestError.CONNECTION_ERROR))
                        } else {
                            requestError.postValue(RequestError(RequestError.REQUEST_ERROR))
                        }
                    })
        )
    }
}