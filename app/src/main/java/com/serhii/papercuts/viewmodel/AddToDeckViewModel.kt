package com.serhii.papercuts.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.serhii.papercuts.managers.ConnectionManager
import com.serhii.papercuts.managers.SharedPreferencesManager
import com.serhii.papercuts.model.entities.*
import com.serhii.papercuts.model.network.ApiRequestService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers

class AddToDeckViewModel(
    var apiRequestService: ApiRequestService,
    var sharedPreferencesManager: SharedPreferencesManager,
    var connectionDetector: ConnectionManager
) : ViewModel() {

    private var compositeDisposable: CompositeDisposable = CompositeDisposable()
    var inProgress = MutableLiveData<Boolean>()
    private val requestError = MutableLiveData<RequestError>()

    val decksLiveData = MutableLiveData<List<DeckEntity>>()
    val successLiveData = MutableLiveData<Boolean>()

    init {
        getDecks()
    }

    fun getDecks() {
        if (connectionDetector.isConnected.value != true) {
            requestError.postValue(RequestError(RequestError.CONNECTION_ERROR))
            return
        }
        inProgress.postValue(true)

        compositeDisposable.add(
            apiRequestService
                .getAllDecks()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                    onSuccess = {
                        inProgress.postValue(false)

                        decksLiveData.value = it.body()
                    },
                    onError = {
                        inProgress.postValue(false)
                        if (connectionDetector.isConnected.value != true) {
                            requestError.postValue(RequestError(RequestError.CONNECTION_ERROR))
                        } else {
                            requestError.postValue(RequestError(RequestError.REQUEST_ERROR))
                        }
                    })
        )
    }

    fun addCardsToDeck(deckId: String, ids: List<String>) {
        if (connectionDetector.isConnected.value != true) {
            requestError.postValue(RequestError(RequestError.CONNECTION_ERROR))
            return
        }
        inProgress.postValue(true)

        compositeDisposable.add(
            apiRequestService
                .addCardsToDeck(deckId, ids)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                    onSuccess = {
                        inProgress.postValue(false)

                        successLiveData.value = true
                    },
                    onError = {
                        inProgress.postValue(false)
                        if (connectionDetector.isConnected.value != true) {
                            requestError.postValue(RequestError(RequestError.CONNECTION_ERROR))
                        } else {
                            requestError.postValue(RequestError(RequestError.REQUEST_ERROR))
                        }
                    })
        )
    }
}