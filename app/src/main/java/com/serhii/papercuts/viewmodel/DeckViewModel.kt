package com.serhii.papercuts.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.serhii.papercuts.managers.ConnectionManager
import com.serhii.papercuts.managers.SharedPreferencesManager
import com.serhii.papercuts.model.entities.*
import com.serhii.papercuts.model.network.ApiRequestService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers

class DeckViewModel(
    var apiRequestService: ApiRequestService,
    var sharedPreferencesManager: SharedPreferencesManager,
    var connectionDetector: ConnectionManager
) : ViewModel() {

    private var compositeDisposable: CompositeDisposable = CompositeDisposable()
    var inProgress = MutableLiveData<Boolean>()
    val requestError = MutableLiveData<RequestError>()

    val deckLiveData = MutableLiveData<DeckEntity>()
    val cardsLiveData = MutableLiveData<List<CardEntity>>()
    var removedCardLiveData = MutableLiveData<List<String>>()
    var deckDeletedLiveData = MutableLiveData<Boolean>()

    fun getDeckCards() {
        if (connectionDetector.isConnected.value != true) {
            requestError.postValue(RequestError(RequestError.CONNECTION_ERROR))
            return
        }
        inProgress.postValue(true)

        compositeDisposable.add(
            apiRequestService
                .getDeckCards(deckLiveData.value?.id ?: "")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                    onSuccess = {
                        inProgress.postValue(false)

                        cardsLiveData.value = it.body()
                    },
                    onError = {
                        inProgress.postValue(false)
                        if (connectionDetector.isConnected.value != true) {
                            requestError.postValue(RequestError(RequestError.CONNECTION_ERROR))
                        } else {
                            requestError.postValue(RequestError(RequestError.REQUEST_ERROR))
                        }
                    })
        )
    }

    fun removeCardsFromDeck(cardIds: List<String>) {
        if (connectionDetector.isConnected.value != true) {
            requestError.postValue(RequestError(RequestError.CONNECTION_ERROR))
            return
        }
        inProgress.postValue(true)

        val id = deckLiveData.value?.id ?: return

        compositeDisposable.add(
            apiRequestService
                .removeCardsFromDeck(id, cardIds)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                    onSuccess = {
                        inProgress.postValue(false)

                        removedCardLiveData.postValue(cardIds)
                    },
                    onError = {
                        inProgress.postValue(false)
                        if (connectionDetector.isConnected.value != true) {
                            requestError.postValue(RequestError(RequestError.CONNECTION_ERROR))
                        } else {
                            requestError.postValue(RequestError(RequestError.REQUEST_ERROR))
                        }
                    })
        )
    }

    fun deleteCards(cardIds: List<String>, isGlobally: Boolean) {
        if (connectionDetector.isConnected.value != true) {
            requestError.postValue(RequestError(RequestError.CONNECTION_ERROR))
            return
        }
        inProgress.postValue(true)

        compositeDisposable.add(
            apiRequestService
                .deleteCards(cardIds, isGlobally)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                    onSuccess = {
                        inProgress.postValue(false)

//                        removedCardLiveData.value = cardId
                    },
                    onError = {
                        inProgress.postValue(false)
                        if (connectionDetector.isConnected.value != true) {
                            requestError.postValue(RequestError(RequestError.CONNECTION_ERROR))
                        } else {
                            requestError.postValue(RequestError(RequestError.REQUEST_ERROR))
                        }
                    })
        )
    }

    fun deleteDeck() {
        if (connectionDetector.isConnected.value != true) {
            requestError.postValue(RequestError(RequestError.CONNECTION_ERROR))
            return
        }
        inProgress.postValue(true)

        compositeDisposable.add(
            apiRequestService
                .deleteDeck(deckLiveData.value?.id ?: "")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                    onSuccess = {
                        inProgress.postValue(false)

                        deckDeletedLiveData.value = true
                    },
                    onError = {
                        inProgress.postValue(false)
                        if (connectionDetector.isConnected.value != true) {
                            requestError.postValue(RequestError(RequestError.CONNECTION_ERROR))
                        } else {
                            requestError.postValue(RequestError(RequestError.REQUEST_ERROR))
                        }
                    })
        )
    }
}