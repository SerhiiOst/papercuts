package com.serhii.papercuts.application

import android.content.Context
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication
import com.serhii.papercuts.koin_modules.adaptersModule
import com.serhii.papercuts.koin_modules.managersModule
import com.serhii.papercuts.koin_modules.networkModule
import com.serhii.papercuts.koin_modules.viewModelModule
import org.koin.android.ext.koin.androidContext

class PapercutsApplication : MultiDexApplication() {

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    override fun onCreate() {
        super.onCreate()
        startKoin()
    }


    private fun startKoin() {
        org.koin.core.context.startKoin {
            //  androidLogger(level = Level.DEBUG)
            androidContext(this@PapercutsApplication)
            modules(
                networkModule,
                viewModelModule,
                adaptersModule,
                managersModule
            )
        }
    }
}