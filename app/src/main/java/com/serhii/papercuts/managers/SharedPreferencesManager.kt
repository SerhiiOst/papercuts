package com.serhii.papercuts.managers

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.content.edit
import com.serhii.papercuts.R
import java.util.concurrent.TimeUnit

class SharedPreferencesManager constructor(context: Context) {

    private val USER_TOKEN = "USER_TOKEN"
    private val APP_THEME = "APP_THEME"
    private val USER_SEEN_FILE_DIALOG = "USER_SEEN_FILE_DIALOG"

    companion object {
        private const val PREFERENCES_FILE_NAME = "papercuts.spref"
    }

    private val sharedPreferences: SharedPreferences

    init {
        this.sharedPreferences =
            context.getSharedPreferences(PREFERENCES_FILE_NAME, Context.MODE_PRIVATE)
    }

    var userAccessToken: String?
        get() = sharedPreferences.getString(USER_TOKEN, null)
        set(token) {
            sharedPreferences.edit {
                putString(USER_TOKEN, token)
            }
        }

    var appTheme: Int
        get() = if (sharedPreferences.getInt(
                APP_THEME,
                0
            ) == 0
        ) AppCompatDelegate.getDefaultNightMode() else sharedPreferences.getInt(APP_THEME, 0)
        set(token) {
            sharedPreferences.edit {
                putInt(APP_THEME, token)
            }
        }

    private var userSeenParseTutorial: Boolean? =
        sharedPreferences.getBoolean(USER_SEEN_FILE_DIALOG, false)

    fun setToken(uId: String) {
        sharedPreferences.edit()
            .putString(USER_TOKEN, uId)
            .apply()
    }

    fun clearToken() {
        sharedPreferences.edit()
            .remove(USER_TOKEN)
            .apply()
    }

    fun changeTheme() {
        val mode = if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_NO)
            AppCompatDelegate.MODE_NIGHT_YES
        else
            AppCompatDelegate.MODE_NIGHT_NO
        appTheme = mode
        AppCompatDelegate.setDefaultNightMode(mode)
    }

    fun getUserSeenFileDialog(): Boolean {
        val v = userSeenParseTutorial ?: false
        if (!v)
            sharedPreferences.edit()
                .putBoolean(USER_SEEN_FILE_DIALOG, true)
                .apply()
        return v
    }

    fun checkToken() = true

    fun clearAllSharedPreferences() {
        sharedPreferences.edit()
            .clear()
            .apply()
    }
}