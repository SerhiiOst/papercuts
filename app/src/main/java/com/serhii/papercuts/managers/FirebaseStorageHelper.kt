package com.serhii.papercuts.managers

import android.content.ContentValues
import android.net.Uri
import android.util.Log
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.serhii.papercuts.utils.extencions.getRemotePath

object FirebaseStorageHelper {

    var storageReference: StorageReference = FirebaseStorage.getInstance().reference

    fun uploadImage(uri: Uri, successListener: OnSuccessListener<Any>, failureListener: OnFailureListener) {
        val riversRef = storageReference.child(uri.getRemotePath())
        val uploadTask = riversRef.putFile(uri)

        uploadTask
            .addOnSuccessListener(successListener)
            .addOnFailureListener(failureListener)
    }
}