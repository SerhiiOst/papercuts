package com.serhii.papercuts.model.entities

import androidx.cardview.widget.CardView
import retrofit2.http.Field
import java.io.Serializable

data class CardEntity(
    val id: String,
    val frontText: String,
    val frontImage: String,
    val backText: String,
    val backImage: String,
    val tags: List<String>,
    val progress: Int,
    val randomSides: Boolean? = null,
    val isAuthor: Boolean? = null,
    val author: String? = null,
    var isLiked: Boolean? = null,
    val created: CreatedEntity? = null
): Serializable

data class CreatedEntity(
    @Field("_seconds")
    val seconds: Long,
    @Field("_nanoseconds")
    val nanoseconds: Long
): Serializable