package com.serhii.papercuts.model.entities

data class ProgressEntity(
    val progress: Int
)