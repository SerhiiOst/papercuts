package com.serhii.papercuts.model.entities

import java.io.Serializable

data class DeckEntity(
    val id: String,
    val name: String,
    val description: String,
    val cardCount: Int,
    val tags: List<TagEntity> = emptyList()
) : Serializable