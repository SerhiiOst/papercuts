package com.serhii.papercuts.model.entities

data class TagsEntity (
    val tags: List<TagEntity>
)