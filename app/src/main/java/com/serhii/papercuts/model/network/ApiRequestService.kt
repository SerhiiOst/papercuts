package com.serhii.papercuts.model.network

import com.serhii.papercuts.model.entities.*
import com.serhii.papercuts.model.network.NetworkUrls
import io.reactivex.Single
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.*

interface ApiRequestService {

    //---- DECKS ----

    @GET(NetworkUrls.DECKS)
    fun getAllDecks(): Single<Response<List<DeckEntity>>>

    @GET(NetworkUrls.DECK)
    fun getDeck(
        @Path("deckId") deckId: String
    ): Single<Response<DeckEntity>>

    @POST(NetworkUrls.DECKS)
    fun addDeck(@Body deck: DeckRequest): Single<ResponseBody>

    @POST(NetworkUrls.DECK_UPDATE)
    fun updateDeck(
        @Path("deckId") deckId: String,
        @Body deck: DeckRequest
    ): Single<ResponseBody>

    @DELETE(NetworkUrls.DECK)
    fun deleteDeck(
        @Path("deckId") deckId: String
    ): Single<ResponseBody>

    @GET(NetworkUrls.DECK_CARDS)
    fun getDeckCards(
        @Path("deckId") deckId: String
    ): Single<Response<List<CardEntity>>>

    @POST(NetworkUrls.DECK_CARDS)
    fun addCardsToDeck(
        @Path("deckId") deckId: String,
        @Body cardIds: List<String>
    ): Single<ResponseBody>

    @POST(NetworkUrls.DECK_CARDS_REMOVE)
    fun removeCardsFromDeck(
        @Path("deckId") deckId: String,
        @Body cardIds: List<String>
    ): Single<ResponseBody>

    @GET(NetworkUrls.DECKS_SEARCH)
    fun searchDecks(
        @Path("query") query: String
    ): Single<Response<List<DeckEntity>>>


    //---- CARDS ----

    @GET(NetworkUrls.CARDS)
    fun getAllCards(): Single<Response<List<CardEntity>>>

    @GET(NetworkUrls.CARDS_USER)
    fun getUserCards(): Single<Response<List<CardEntity>>>

    @GET(NetworkUrls.CARDS_USER_ARCHIVE)
    fun getUserArchive(): Single<Response<List<CardEntity>>>

    @GET(NetworkUrls.CARDS_USER_SEARCH)
    fun searchUserCards(
        @Query("q") q: String
    ): Single<Response<List<CardEntity>>>

    @GET(NetworkUrls.CARDS_USER_SEARCH_TAGS)
    fun searchUserCardsByTags(
        @Query("tags") tags: String
    ): Single<Response<List<CardEntity>>>

    @POST(NetworkUrls.CARDS)
    fun addCard(@Body card: CardRequest): Single<ResponseBody>

    @POST(NetworkUrls.CARDS_PACK)
    fun addCardsPack(@Body card: CardsPackEntity): Single<ResponseBody>

    @POST(NetworkUrls.CARD_UPDATE)
    fun updateCard(
        @Path("cardId") cardId: String,
        @Body card: CardEntity
    ): Single<ResponseBody>

    @POST(NetworkUrls.CARDS_DELETE)
    fun deleteCards(
        @Body cardIds: List<String>,
        @Query("deleteGlobally") deleteGlobally: Boolean
    ): Single<ResponseBody>

    @POST(NetworkUrls.CARD_REMEMBER)
    fun rememberCard(
        @Query("id") id: String,
        @Query("p") progress: Int
    ): Single<ResponseBody>

    @POST(NetworkUrls.CARD_FORGET)
    fun forgetCard(
        @Query("id") id: String
    ): Single<ResponseBody>

    @POST(NetworkUrls.CARD_LIKE)
    fun likeCard(
        @Body card: CardEntity
    ): Single<ResponseBody>

    @GET(NetworkUrls.PROGRESS)
    fun getCardProgress(
        @Query("tags") tags: String
    ): Single<Response<List<ProgressEntity>>>

    //---- TAGS ----

    @GET(NetworkUrls.TAGS)
    fun getAllTags(): Single<Response<List<String>>>

    @GET(NetworkUrls.TAGS_USER)
    fun getUserTags(): Single<Response<List<String>>>

    @GET(NetworkUrls.TAGS_SEARCH)
    fun searchTags(
        @Path("query") query: String
    ): Single<Response<List<String>>>
}