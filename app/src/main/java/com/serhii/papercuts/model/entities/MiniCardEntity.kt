package com.serhii.papercuts.model.entities

data class MiniCardEntity (
    val frontText: String,
    val backText: String
)