package com.serhii.papercuts.model.network

object NetworkUrls {

    const val BASE_URL = "https://us-central1-papercuts-9701a.cloudfunctions.net/api/"
    //TEST URL
//    const val BASE_URL = "http://localhost:5000/papercuts-9701a/us-central1/"

    const val DECKS = "decks"
    const val DECK = "decks/{deckId}"
    const val DECK_UPDATE = "decks/{deckId}/update"
    const val DECK_CARDS = "decks/{deckId}/cards"
    const val DECK_CARDS_REMOVE = "decks/{deckId}/cards/remove"
    const val DECKS_SEARCH = "decks/search/{query}"

    const val CARDS = "cards"
    const val CARDS_DELETE = "cards/delete"
    const val CARDS_PACK = "cards/pack"
    const val CARD = "cards/{cardId}/"
    const val CARD_LIKE = "cards/like/"
    const val CARD_UPDATE = "cards/update/{cardId}/"
    const val CARD_REMEMBER = "cards/remember"
    const val CARD_FORGET = "cards/forget"
    const val CARDS_USER_SEARCH = "user/cards/search/"
    const val CARDS_USER = "user/cards"
    const val CARDS_USER_ARCHIVE = "user/archive"
    const val CARDS_USER_SEARCH_TAGS = "user/cards/search/tag/"

    const val PROGRESS = "user/stats/"

    const val TAGS = "tags"
    const val TAGS_USER = "user/tags"
    const val TAGS_SEARCH = "tags/search/{query}"
}