package com.serhii.papercuts.model.entities

import java.io.Serializable

data class TagEntity(
    val value: String
): Serializable {
    override fun toString(): String {
        return value
    }
}