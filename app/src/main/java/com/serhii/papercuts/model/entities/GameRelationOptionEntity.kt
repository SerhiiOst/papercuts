package com.serhii.papercuts.model.entities

data class GameRelationOptionEntity(
    val value: String,
    var state: RelationOptionState = RelationOptionState.IDLE
)

enum class RelationOptionState {
    IDLE,
    SELECTED,
    WRONG,
    RIGHT,
    ANSWERED
}