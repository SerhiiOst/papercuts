package com.serhii.papercuts.model.entities

data class CardsPackEntity(
    val cards: List<MiniCardEntity>,
    val randomSides: Boolean,
    val tags: List<String>
)