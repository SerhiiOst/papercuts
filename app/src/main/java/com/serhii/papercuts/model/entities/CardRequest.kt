package com.serhii.papercuts.model.entities

data class CardRequest(
    val frontText: String,
    val frontImage: String,
    val backText: String,
    val backImage: String,
    val randomSides: Boolean,
    val tags: List<String>
)