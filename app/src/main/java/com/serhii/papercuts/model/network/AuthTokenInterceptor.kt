package com.serhii.papercuts.model.network

import android.content.Context
import com.serhii.papercuts.managers.SharedPreferencesManager
import okhttp3.Interceptor
import okhttp3.Response

/**
 * Created by AlexLampa on 31.05.2018.
 */

class AuthInterceptor constructor(
    val context: Context
) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val token = SharedPreferencesManager(context).userAccessToken

        val request = if (token.isNullOrEmpty()) {
            chain.request()
        } else {
            chain.request().newBuilder()
//                .addHeader("Authorization", "Bearer $token")
                .addHeader("Authorization", token)
                .build()
        }
        return chain.proceed(request)
    }
}
