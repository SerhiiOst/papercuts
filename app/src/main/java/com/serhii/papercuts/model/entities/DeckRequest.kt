package com.serhii.papercuts.model.entities

data class DeckRequest(
    val name: String,
    val description: String
)