package com.serhii.papercuts.utils.view

import android.content.Context
import android.util.AttributeSet
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import com.serhii.papercuts.R
import kotlinx.android.synthetic.main.item_like.view.*


class GlobalFlashCardView(ctx: Context, attrs: AttributeSet) : FlashCardView(ctx, attrs) {

    private var frontLikeView: View =
        LayoutInflater.from(context).inflate(R.layout.item_like, this, false)
    private var backLikeView =
        LayoutInflater.from(context).inflate(R.layout.item_like, this, false)

    var onLikeClick: ((String) -> Unit)? = null

    override fun bindCard() {
        super.bindCard()

        if(cardEntity?.isLiked == true) {
            frontLikeView.likeBtn.isEnabled = false
            backLikeView.likeBtn.isEnabled = false
            frontLikeView.likeBtn.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_check_circle))
            backLikeView.likeBtn.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_check_circle))
        } else {
            frontLikeView.likeBtn.setOnClickListener {
                onLikeClick?.invoke(cardEntity?.id ?: "")
                setLoading(true)
            }
            backLikeView.likeBtn.setOnClickListener {
                onLikeClick?.invoke(cardEntity?.id ?: "")
                setLoading(true)
            }
        }
        setLoading(false)

        backContent.addView(backLikeView)
        frontContent.addView(frontLikeView)
    }

    private fun setLoading(value: Boolean) {
        frontLikeView.likeBtn.isVisible = !value
        backLikeView.likeBtn.isVisible = !value
        frontLikeView.likeProgressBar.isVisible = value
        backLikeView.likeProgressBar.isVisible = value
    }
}