package com.serhii.papercuts.utils.view

import android.content.Context
import android.util.AttributeSet

class SwappableFlashCardView(ctx: Context, attrs: AttributeSet) : FlashCardView(ctx, attrs) {
    override fun bindCard() {
        frontCard.cardElevation = cardElevation
        backCard.cardElevation = 0f
        frontCard.rotationY = 0f
        backCard.rotationY = -180f
        backCard.alpha = 0f
        frontCard.alpha = 1f
        if (cardEntity?.randomSides == true && (0..1).random() == 1) {
            backCard.cardElevation = cardElevation
            frontCard.cardElevation = 0f
            backCard.rotationY = 0f
            frontCard.rotationY = -180f
            backCard.alpha = 1f
            frontCard.alpha = 0f
        }
        super.bindCard()
    }
}