package com.serhii.papercuts.utils.view

import android.content.Context
import android.util.AttributeSet
import androidx.core.content.ContextCompat
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.serhii.papercuts.R

class PapercutsSwipeRefreshLayout(ctx: Context, attrs: AttributeSet): SwipeRefreshLayout(ctx, attrs) {
    init {
        setProgressBackgroundColorSchemeResource(R.color.background_ternary)
        setColorSchemeColors(
            ContextCompat.getColor(context, R.color.dark_light),
            ContextCompat.getColor(context, R.color.colorAccent),
            ContextCompat.getColor(context, R.color.colorPrimary)
        )
    }
}