package com.serhii.papercuts.utils.extencions

import android.animation.AnimatorInflater
import android.animation.AnimatorSet
import android.annotation.SuppressLint
import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.net.Uri
import android.text.TextUtils
import android.util.Patterns
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.PopupMenu
import android.widget.Toast
import androidx.core.animation.doOnEnd
import androidx.core.app.ActivityCompat.startActivityForResult
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import com.jakewharton.rxbinding4.view.clicks
import com.serhii.papercuts.R
import com.serhii.papercuts.model.entities.MiniCardEntity
import com.serhii.papercuts.utils.view.BaseActivity
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.Disposable
import java.io.BufferedReader
import java.io.File
import java.io.FileReader
import java.io.Serializable
import java.lang.Exception
import java.lang.reflect.Method
import java.util.concurrent.TimeUnit


fun Activity.hideKeyBoard(): Unit {
    val view = this.currentFocus
    if (view != null) {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
        imm?.hideSoftInputFromWindow(view.windowToken, 0)
        view.clearFocus()
    }
}

fun Context.showToast(text: String, duration: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this, "" + text, duration).show()
}

inline fun <reified T : BaseActivity> Fragment.getActivity(): T? =
    if (activity != null && activity is T)
        activity as T
    else
        null

fun String.validateOnEmail() =
    !TextUtils.isEmpty(this) && Patterns.EMAIL_ADDRESS.matcher(this).matches()

fun String.validateOnPhoneNumber() =
    !TextUtils.isEmpty(this) && Patterns.PHONE.matcher(this).matches()

fun Uri.getRemotePath() =
    "images/${lastPathSegment}"

fun flip(front: View, back: View) {
    front.isClickable = false
    back.isClickable = false

    val animationOut = AnimatorInflater.loadAnimator(
        front.context,
        R.animator.flip_out
    ) as AnimatorSet
    val animationIn = AnimatorInflater.loadAnimator(
        back.context,
        R.animator.flip_in
    ) as AnimatorSet

    animationOut.doOnEnd { front.isClickable = true }
    animationIn.doOnEnd { back.isClickable = true }

    animationOut.setTarget(front)
    animationIn.setTarget(back)

    animationOut.start()
    animationIn.start()
}

fun dpToPx(dp: Float) = (dp * Resources.getSystem().displayMetrics.density).toInt()

fun pxToDp(px: Float) = (px / Resources.getSystem().displayMetrics.density)

fun Activity.openChooser(code: Int, type: String) {
    val intent = Intent(Intent.ACTION_GET_CONTENT)
    intent.type = type
    intent.addCategory(Intent.CATEGORY_OPENABLE)

    try {
        startActivityForResult(
            Intent.createChooser(intent, "Select a File"),
            code
        )
    } catch (ex: ActivityNotFoundException) {
        // Potentially direct the user to the Market with a Dialog
        Toast.makeText(
            this, "Please install a File Manager.",
            Toast.LENGTH_SHORT
        ).show()
    }
}

fun File.toCardsListMethod1() : ArrayList<MiniCardEntity> {
    val reader = FileReader(this)
    val miniCards = ArrayList<MiniCardEntity>()
    try {
        val br = BufferedReader(reader)
        var line = br.readLine()
        while (line != null) {
            val sides = line.split(" -- ")
            if (sides.size == 2) {
                miniCards.add(MiniCardEntity(sides[0], sides[1]))
            }
            line = br.readLine()
        }
    } catch (e: Exception) {
    }
    return miniCards
}

fun File.toCardsListMethod2() : ArrayList<MiniCardEntity> {
    val reader = FileReader(this)
    val miniCards = ArrayList<MiniCardEntity>()
    val fronts = ArrayList<String>()
    val backs = ArrayList<String>()
    try {
        val br = BufferedReader(reader)
        var line = br.readLine()
        var isFront = true
        while (line != null) {
            if(line.isNotBlank()) {
                if (isFront) {
                    if (line == "---")
                        isFront = false
                    else
                        fronts.add(line)
                } else
                    backs.add(line)
            }
            line = br.readLine()
        }
        if(fronts.size == backs.size)
            for(i in 0 until fronts.size)
                miniCards.add(MiniCardEntity(fronts[i], backs[i]))
    } catch (e: Exception) {
    }
    return miniCards
}


fun PopupMenu.setupIcons() {
    try {
        val fields = javaClass.declaredFields
        for (field in fields) {
            if ("mPopup" == field.name) {
                field.isAccessible = true
                val menuPopupHelper = field.get(this)
                val classPopupHelper = Class.forName(
                    menuPopupHelper
                        .javaClass.name
                )
                val setForceIcons: Method = classPopupHelper.getMethod(
                    "setForceShowIcon", Boolean::class.javaPrimitiveType
                )
                setForceIcons.invoke(menuPopupHelper, true)
                break
            }
        }
    } catch (e: Exception) {
        e.printStackTrace()
    }
}

fun <T: Serializable> MutableLiveData<List<T>>.addItems(array: ArrayList<T>) {
    value?.let {
        val arr = ArrayList(it)
        arr.addAll(array)
        value = arr
    }
}

fun <T: Serializable> MutableLiveData<List<T>>.removeItems(array: ArrayList<T>) {
    value?.let {
        val arr = it.filterNot { t -> array.contains(t) }
        value = arr
    }
}

@SuppressLint("CheckResult")
fun View.onThrottleClick(windowDuration: Long = 250, subscribeFunction: () -> Unit): Disposable =
    clicks()
        .throttleFirst(windowDuration, TimeUnit.MILLISECONDS)
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe {
            subscribeFunction.invoke()
        }