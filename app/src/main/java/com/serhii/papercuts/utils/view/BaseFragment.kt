package com.serhii.papercuts.utils.view

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.serhii.papercuts.R
import com.serhii.papercuts.model.eventbus.eventmodels.EventModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy

abstract class BaseFragment : Fragment() {

    open val title: Int = R.string.app_name
    open val backButtonEnabled: Boolean = false

    private val disposable = CompositeDisposable()

    fun getFragmentName(): String {
        return this.javaClass.simpleName
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

//        disposable.add(
//            MessageEventBus.INSTANCE
//                .toObservable()
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribeBy(
//                    onNext = {
//                        if (it is RefreshEventModel) {
//                            refresh()
//                        }
//                    }
//                )
//        )
    }

    open fun refresh() {}

    fun stopRefresh() {
    }

    class RefreshEventModel: EventModel
}