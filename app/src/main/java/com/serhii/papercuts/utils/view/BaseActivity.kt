package com.serhii.papercuts.utils.view

import android.graphics.Color
import android.util.TypedValue
import android.view.MenuItem
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.serhii.papercuts.R

abstract class BaseActivity : AppCompatActivity() {

    protected open var fragmentContainerId = 0

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    fun getActivityName(): String {
        return this.javaClass.simpleName
    }

    fun upgradeStatusBar() {
//        window.setFlags(
//            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
//            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
//        )
        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

        // finally change the color
        val a = TypedValue()
        theme.resolveAttribute(android.R.attr.windowBackground, a, true)
        window.statusBarColor = a.data
    }
}