package com.serhii.papercuts.utils.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewOutlineProvider
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.google.android.material.card.MaterialCardView
import com.serhii.papercuts.R
import com.serhii.papercuts.managers.FirebaseStorageHelper
import com.serhii.papercuts.model.entities.CardEntity
import com.serhii.papercuts.utils.extencions.flip

open class FlashCardView//        super
//        super(context, attrs)
    (context: Context, attrs: AttributeSet) : FrameLayout(context, attrs) {

    var cardEntity: CardEntity? = null
        set(value) {
            field = value
            bindCard()
        }

    var onLongClick: (() -> Boolean)? = null
    var onClick: (() -> Unit)? = null
    val flipFun = {
        if (frontCard.alpha == 0f)
            flip(backCard, frontCard)
        else
            flip(frontCard, backCard)
    }

    protected val strokeWidth: Float
    protected val strokeColor: Int
    protected val cardElevation: Float
    protected val cardMargin: Float
    protected val cornerRadius: Float

    protected val frontCard = MaterialCardView(context)
    protected val backCard = MaterialCardView(context)
    protected val frontContent = FrameLayout(context)
    protected val backContent = FrameLayout(context)

    init {
        context.theme.obtainStyledAttributes(
            attrs,
            R.styleable.FlashCardView,
            0, 0
        ).apply {
            strokeWidth = getDimension(R.styleable.FlashCardView_strokeWidth, 0f)
            strokeColor = getColor(R.styleable.FlashCardView_strokeColor, 0)
            cardElevation = getDimension(R.styleable.FlashCardView_cardElevation, 6f)
            cardMargin = getDimension(R.styleable.FlashCardView_cardMargin, 0f)
            cornerRadius = getDimension(R.styleable.FlashCardView_cornerRadius, 0f)
            recycle()
        }
        frontCard.layoutParams = LayoutParams(
            LayoutParams.MATCH_PARENT,
            LayoutParams.MATCH_PARENT
        )
        frontCard.strokeColor = strokeColor
        frontCard.strokeWidth = strokeWidth.toInt()
        frontCard.cardElevation = cardElevation
        frontCard.radius = cornerRadius
        frontCard.outlineProvider = null
        frontCard.setCardBackgroundColor(ContextCompat.getColor(context, R.color.card_background))
        frontCard.setOnLongClickListener { onLongClick?.invoke() ?: true }
        frontCard.addView(frontContent.also {
            it.layoutParams = LayoutParams(
                LayoutParams.MATCH_PARENT,
                LayoutParams.MATCH_PARENT
            )
        })
        backCard.layoutParams = LayoutParams(
            LayoutParams.MATCH_PARENT,
            LayoutParams.MATCH_PARENT
        )
        backCard.strokeColor = strokeColor
        backCard.strokeWidth = strokeWidth.toInt()
        backCard.setCardBackgroundColor(ContextCompat.getColor(context, R.color.card_background))
        backCard.cardElevation = 0f
        backCard.outlineProvider = null
        backCard.radius = cornerRadius
        backCard.alpha = 0f
        backCard.setOnLongClickListener { onLongClick?.invoke() ?: true }
        backCard.addView(backContent.also {
            it.layoutParams = LayoutParams(
                LayoutParams.MATCH_PARENT,
                LayoutParams.MATCH_PARENT
            )
        })
        addView(frontCard)
        addView(backCard)
        onClick = flipFun
        frontCard.setOnClickListener { onClick?.invoke() }
        backCard.setOnClickListener { onClick?.invoke() }
    }

    protected open fun bindCard() {
        cardEntity?.let {
            val scale = resources.displayMetrics.density * 8000
            frontCard.cameraDistance = scale
            backCard.cameraDistance = scale

            //to prevent content overlap
            frontContent.removeAllViews()
            backContent.removeAllViews()

            //TODO simplify
            val frontView: View
            if (it.frontImage.isNotEmpty()) {
                frontView = LayoutInflater.from(context)
                    .inflate(R.layout.item_card_content_image, frontContent, false)
                FirebaseStorageHelper.storageReference.child(it.frontImage)
                    .downloadUrl
                    .addOnSuccessListener { uri ->
                        Glide.with(context)
                            .load(uri)
                            .into(frontView as ImageView)
                    }.addOnFailureListener { ex ->
                        ex.printStackTrace()
                    }
            } else {
                frontView = LayoutInflater.from(context)
                    .inflate(R.layout.item_card_content_text, frontContent, false)
                (frontView as TextView).text = cardEntity?.frontText?.replace("\\n", "\n")
            }
            frontContent.addView(frontView)

            val backView: View
            if (it.backImage.isNotEmpty()) {
                backView = LayoutInflater.from(context)
                    .inflate(R.layout.item_card_content_image, backContent, false)
                FirebaseStorageHelper.storageReference.child(it.backImage)
                    .downloadUrl
                    .addOnSuccessListener { uri ->
                        Glide.with(context)
                            .load(uri)
                            .into(backView as ImageView)
                    }
            } else {
                backView = LayoutInflater.from(context)
                    .inflate(R.layout.item_card_content_text, backContent, false)
                (backView as TextView).text = cardEntity?.backText?.replace("\\n", "\n")
            }
            backContent.addView(backView)
        }
    }

    fun resetCard(swappable: Boolean) {
        frontCard.cardElevation = cardElevation
        backCard.cardElevation = 0f
        frontCard.rotationY = 0f
        backCard.rotationY = -180f
        backCard.alpha = 0f
        frontCard.alpha = 1f
        if (swappable && (0..1).random() == 1) {
            backCard.cardElevation = cardElevation
            frontCard.cardElevation = 0f
            backCard.rotationY = 0f
            frontCard.rotationY = -180f
            backCard.alpha = 1f
            frontCard.alpha = 0f
        }
    }

    fun toggleSelect(value: Boolean = true) {
        val color = if (value) R.color.card_background_selected else R.color.card_background
        frontCard.setCardBackgroundColor(ContextCompat.getColor(context, color))
        backCard.setCardBackgroundColor(ContextCompat.getColor(context, color))
    }
}