package com.serhii.papercuts.utils

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView

abstract class BaseAdapter<T> : RecyclerView.Adapter<BaseAdapter.BaseViewHolder>() {

    @LayoutRes
    protected open val layoutId: Int = android.R.layout.test_list_item
    protected open val testItemCount = 0
    protected open val notifyAutomatically = true

    var items: List<T>? = null
        set(value) {
            field = value
            if (notifyAutomatically)
                notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(layoutId, parent, false)
        return BaseViewHolder(v)
    }

    override fun getItemCount() = items?.size ?: testItemCount

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        items?.get(position)?.let {
            bind(holder.v, it, position)
        }
    }

    class BaseViewHolder(val v: View) : RecyclerView.ViewHolder(v)

    protected abstract fun bind(itemView: View, entity: T, position: Int)
}