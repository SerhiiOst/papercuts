package com.serhii.papercuts.utils.view

import androidx.fragment.app.DialogFragment

abstract class BaseDialogFragment : DialogFragment() {

    fun getFragmentName(): String {
        return this.javaClass.simpleName
    }
}