package com.serhii.papercuts.koin_modules

import com.serhii.papercuts.ui.adapters.recycler.*
import org.koin.dsl.module

val adaptersModule = module {

    factory { CardAdapter() }
    factory { CardExploreAdapter() }
    factory { SelectCardAdapter() }
    factory { DeckAdapter() }
    factory { DeckCardAdapter() }
    factory { StudyAdapter() }
    factory { TagAdapter() }

    factory { GameRelationOptionAdapter() }
}