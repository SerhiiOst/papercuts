package com.serhii.papercuts.koin_modules

import com.serhii.papercuts.managers.ConnectionManager
import com.serhii.papercuts.managers.SharedPreferencesManager
import org.koin.dsl.module

val managersModule = module {

    single { SharedPreferencesManager(get()) }

    single(createdAtStart = true) { ConnectionManager(get()) }

}