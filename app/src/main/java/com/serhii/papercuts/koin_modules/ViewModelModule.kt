package com.serhii.papercuts.koin_modules

import com.serhii.papercuts.viewmodel.*
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {

    viewModel { AuthViewModel(get(), get(), get()) }
    viewModel { LibraryViewModel(get(), get(), get()) }
    viewModel { DeckViewModel(get(), get(), get()) }
    viewModel { CreateCardViewModel(get(), get(), get()) }
    viewModel { CreateDeckViewModel(get(), get(), get()) }
    viewModel { ExploreViewModel(get(), get(), get()) }
    viewModel { StudyViewModel(get(), get(), get()) }
    viewModel { SearchResultViewModel(get(), get(), get()) }
    viewModel { SearchDecksViewModel(get(), get(), get()) }
    viewModel { ProgressViewModel(get(), get(), get()) }
    viewModel { ArchiveViewModel(get(), get(), get()) }
    viewModel { AddToDeckViewModel(get(), get(), get()) }
    viewModel { SelectCardsViewModel(get(), get(), get()) }

    viewModel { GameRelationViewModel(get(), get(), get()) }
}