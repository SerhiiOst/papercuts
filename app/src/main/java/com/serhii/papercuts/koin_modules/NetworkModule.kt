package com.serhii.papercuts.koin_modules

import android.content.Context
import com.google.gson.GsonBuilder
import com.readystatesoftware.chuck.ChuckInterceptor
import com.serhii.papercuts.model.network.ApiRequestService
import com.serhii.papercuts.model.network.AuthInterceptor
import com.serhii.papercuts.model.network.NetworkUrls
import okhttp3.Cache
import okhttp3.OkHttpClient
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val networkModule = module {

    single { Cache((get() as Context).cacheDir, 20L * 1024 * 1024) }

    single {
        ChuckInterceptor(get())
    }

    single {
        OkHttpClient.Builder()
            .connectTimeout(15L, TimeUnit.SECONDS)
            .readTimeout(15L, TimeUnit.SECONDS)
            .writeTimeout(15L, TimeUnit.SECONDS)
            .addInterceptor(AuthInterceptor(get()))
            .addNetworkInterceptor(get() as ChuckInterceptor)
            .cache(get())
            .build()
    }

    single { GsonBuilder().setLenient().create() }

    single {
        Retrofit.Builder()
            .client(get())
            .baseUrl(NetworkUrls.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(get()))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
    }

    single { (get() as Retrofit).create(ApiRequestService::class.java) }

}